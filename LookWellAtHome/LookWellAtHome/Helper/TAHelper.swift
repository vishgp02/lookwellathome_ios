//
//  File.swift
//  Venue
//
//  Created by Shashank Gupta on 23/11/17.
//  Copyright © 2017 TechAhead. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import SVProgressHUD
import MobileCoreServices
//import SwiftyJSON

//MARK: Helper Methods

let screenScaleFactor : CGFloat = UIScreen.main.bounds.size.width / 375.0
let window = UIApplication.shared.keyWindow!
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var chatWithUser = ""
//var JsonData : JSON?

class TAHelper : NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    static let sharedInstance: TAHelper = {
        
        let instance = TAHelper()
        return instance
    }()
    var imagePickerHanler: ((_ info: [String : AnyObject]?, _ imagePickerHanler: UIImage?)-> Void)?
    var currentViewController: UIViewController?
 
    class func startLoader(_ text:String)
    {
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: text)
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        }
    }
    
    
    func registerCell(tbl:UITableView,nibName:String)
    {
        tbl.register(UINib(nibName:nibName, bundle:nil), forCellReuseIdentifier:nibName)
    }
    
    func registerCollectionViewCell(collectionVW:UICollectionView,nibName:String)
    {
    
        collectionVW.register(UINib(nibName:nibName, bundle:nil), forCellWithReuseIdentifier:nibName)
    }
    
    class func performTaskInMainQueue(task:@escaping () -> ()) {
        DispatchQueue.main.async {
            task()
        }
    }
    
    class func performTaskAfterDealy(_ timeInteval: TimeInterval, _ task:@escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: (.now() + timeInteval)) {
            task()
        }
    }
    
    class func getUniqueId() -> String {
        return UUID().uuidString
    }
    class func prepareNavigationBarButton(imageName : String, target : UIViewController?, action : Selector?) -> UIBarButtonItem
    {
        if imageName != ""
        {
            let rightImage = UIImage(named: imageName)!.withRenderingMode(.alwaysOriginal)
            let barButton = UIBarButtonItem(image: rightImage, style:.plain, target: target, action: action)
            return barButton
        }
        else
        {
            let barButton = UIBarButtonItem.init(title: "", style: .plain, target: target, action: action)
            return barButton
        }
    }
    
    class func stopLoader()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    class func addPullToRefreshOn(view: UIView, target: UIViewController, selector: Selector) -> UIRefreshControl
    {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        view.addSubview(refreshControl)
        return refreshControl
    }

//    class func setViewShadow(view : UIView ,color : CGColor,corner : CGFloat)
//    {
//        view.layer.masksToBounds = false
//        view.layer.borderColor = color
//        view.layer.shadowOffset = CGSize(width: 0, height: 0)
//        view.layer.shadowRadius = 5
//        view.layer.cornerRadius = corner*screenScaleFactor
//        view.layer.shadowOpacity = 0.2
//    }
//    
//    

    class func topMostController() -> UIViewController? {
        
        let topController =  UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = topController as? UINavigationController,
            let activeViewC = navigationController.visibleViewController {
            return activeViewC
        }
        if let viewC = topController {
            return viewC
        }
        return nil
    }

    
    class func getIndexPathFor(view: UIView, tableView: UITableView) -> IndexPath? {
        
        let point = tableView.convert(view.bounds.origin, from: view)
        let indexPath = tableView.indexPathForRow(at: point)
        return indexPath
    }
    
    class func getIndexPathFor(view: UIView, collectionView: UICollectionView) -> NSIndexPath? {
        
        let point = collectionView.convert(view.bounds.origin, from: view)
        let indexPath = collectionView.indexPathForItem(at: point)
        return indexPath as NSIndexPath?
    }
    
    class func isValidEmail(_ text: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: text)
        if valid {
            valid = !text.contains("..")
        }
        return valid
    }
    
    class func isValidPhone(phone: String) -> Bool {
        
        //        let phoneRegex = "^((\\+)|(00))[0-9]{6,14}$";
        let phoneRegex = "^[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    
    class func isValidPassword(password: String) -> Bool {
        if password.length >= 6 && password.length <= 20
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    class func isString(object:AnyObject) -> Bool
    {
        return object is String
    }
    
    class func isNumeric(str:String) -> Bool
    {
        let num = Int(str)
        if num != nil
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    class func getStringFromDictionary(aDict : Dictionary<String,AnyObject>?, key : String) -> String
    {
        guard let dict = aDict,
            let _ = dict[key]
            else
        {
            return ""
            
        }
        
        let strVal = self.convertToString(obj: dict[key])
        return strVal
    }
    
    class func convertToString(obj : Any?) -> String
    {
        if let str = obj as? String
        {
            return String(format: "%@", str)
        }
        
        if let num = obj as? NSNumber
        {
            return String(format: "%@", num)
        }
        
        let strBlank = ""
        
        if let aString = obj as? String
        {
            return aString
        }
        else
        {
            if obj is String == true || obj is NSNumber == true || obj is Bool == true ||  obj is Int == true
            {
                var aString = String.init(format: "%@", obj as! CVarArg)
                if aString == "(null)"
                {
                    aString = "0"
                }
                return aString
            }
        }
        
        return strBlank
    }
    
    
    class func setLocalizedText(tableName:String, text:String) -> String
    {
        return NSLocalizedString(text, tableName: tableName, bundle: Bundle.main, value: "", comment: "")
    }
    
    class func setSafeString(_ key:String, value:String?, toDictionary dict:inout [String:AnyObject]){
        
        if let valueString = value {
            dict[key] = valueString as AnyObject?
        }else{
            dict[key] = "" as AnyObject?
        }
    }
    
    class func setSafeInteger(_ key:String, value:Int?, toDictionary dict:inout [String:AnyObject]){
        
        if let numberValue = value {
            dict[key] = String(numberValue) as AnyObject
        }else{
            dict[key] = NSNull()
        }
    }
    
    class func setSafeNumber(_ key:String, value:NSNumber?, toDictionary dict:inout [String:AnyObject]){
        
        if let numberValue = value {
            dict[key] = numberValue.stringValue as AnyObject?
        }else{
            dict[key] = NSNull()
        }
    }
    
    class func setSafeBoolean(_ key:String, value:NSNumber?, toDictionary dict:inout [String:AnyObject]){
        
        if let numberValue = value {
            
            if numberValue.boolValue == true {
                dict[key] = "true" as AnyObject?
            }else{
                dict[key] = "false" as AnyObject?
            }
            
        }else{
            dict[key] = NSNull()
        }
    }
    
    
    class func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func isInternet()
    {
        //let aa = NetworkReachabilityManager.is
    }
    
    class func phoneCallto(phoneNo: String)
    {
        if phoneNo == "" {
            return
        }
        
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNo)")
        {
            if (UIApplication.shared.canOpenURL(phoneCallURL as URL)) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
//
//    class func handleRefreshControlrIfNoNetwrk(refreshCrt: UIRefreshControl,ViewContr: UIViewController?)-> Bool
//    {
//        if !TAHelper.isInternetAvailable()
//        {
//            refreshCrt.endRefreshing()
//
//            let deadlineTime = DispatchTime.now() + 0.1
//            DispatchQueue.main.asyncAfter(deadline: deadlineTime)
//            {
//                TAAlert.showOkAlertFor(title:"", message:ConstantTextsApi.noInternetConnectionTryAgain.localizedString, obj: ViewContr, completion: nil)
//            }
//            return false
//        }
//        return true
//    }
    
    class func getFirstNameFromUsrName(usrName: String)-> String
    {
        if usrName == "" {
            return ""
        }
        let arrOfComp = usrName.components(separatedBy: " ")
        return arrOfComp.first! + "'s "
    }
    
    class func reverseTheString(strToRev: String)-> String
    {
        var arr = [Character]()
        for chrter in strToRev
        {
            arr.append(chrter)
        }
        var newStr = ""
        var len = arr.count
        while len > 0 {
            
            let char = arr[len-1]
            let str = String(char)
            newStr = newStr + str
            len = len - 1
        }
        
        return newStr
    }
    
    
    class func encodeStringForSpaceAndHtml(str: String)->String
    {
        return str.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
//    class func roundOfDate(theDate: Date, toRounded: Int)-> Date
//    {
//        let fromMinute = theDate.minute()
//        
//        let reminder = fromMinute % toRounded
//        
//        if reminder != 0
//        {
//            return theDate.dateByAddingHours(hours: 0, minutes: -reminder, seconds: 0)
//        }
//        
//        return theDate
//    }
    
    class func getDateFromTimeStamp(timeStamp : Double) -> String
    {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
        
    }

    class func getTimeFromTimeStamp(timeStamp : Double) -> String
    {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
        
    }

    class func getCommentTimeFromTimeStamp(timeStamp : Double) -> String
    {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
        
    }

    class func currentMessageTimestamp() -> Int
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd-HHmmss"
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        formatter.timeZone = NSTimeZone.local
        let date = Date()
        let strDate: String = formatter.string(from: date)
        let dateForSetion: Date? = formatter.date(from: strDate)
        let timeSection = Int((dateForSetion?.timeIntervalSince1970)!)
        return timeSection
    }
    
    class func convertServerTimeToTimeStamp(_ dateVal : String)->Int
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatter.date(from: dateVal) else {
            return 0
        }
        let strDate: String = dateFormatter.string(from: date)
        let dateForSetion: Date? = dateFormatter.date(from: strDate)
        let timeSection = Int((dateForSetion?.timeIntervalSince1970)!)
        return timeSection
    }
    
    
    class func getChatMsgCreatedAtTime()->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = Date()
        let strDate: String = dateFormatter.string(from: date)
        return strDate
    }
    
    class func sectionIndetifierDayTimeStamp() -> Int
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        formatter.timeZone = NSTimeZone.local
        let date = Date()
        let strDate: String = formatter.string(from: date)
        let dateForSetion: Date? = formatter.date(from: strDate)
        let timeSection = Int((dateForSetion?.timeIntervalSince1970)!)
        return timeSection
    }
    
    class func stringSectionDate2(timeStamp: TimeInterval) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM, yyyy"
        formatter.locale = Locale(identifier: "en_US")
        let date = Date(timeIntervalSince1970: timeStamp)
        let strDate: String = formatter.string(from: date)
        return strDate
    }
    
    class func stringMessageTime(timeStamp: TimeInterval) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.locale = Locale(identifier: "en_US")
        let date = Date(timeIntervalSince1970: timeStamp)
        let strDate: String = formatter.string(from: date)
        return strDate
    }
    
    class func getExactDateFromString(dateVal:String) -> String
    {
       // let string = "2017-01-27T18:36:36Z"
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatter.date(from: dateVal) else {
            return ""
        }
        //dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = tempLocale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        return dateString
    }
    
    
    class func getDateandTimeFromString(dateVal:String) -> String
    {
        // let string = "2017-01-27T18:36:36Z"
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatter.date(from: dateVal) else {
            return ""
        }
        //dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.dateFormat = "dd MMM yyyy | hh:mm a"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        return dateString
    }
    
    
    class  func convertDobDateFormat(date:String)->String
    {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        return dateString
        
    }
    
    
    
    
  class  func getTopMostViewController() -> UIViewController {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            // topController should now be your topmost view controller
            return topController
        }
        return UIViewController()
    }
    
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController? {
        if let navigationController = rootViewController as? UINavigationController {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        if let tabBarController = rootViewController as? UITabBarController {
            if let selectedTabBarController = tabBarController.selectedViewController {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        
        if let presentedViewController = rootViewController.presentedViewController {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }
    
    class func relativePast(for date : Date) -> String
    {
        
        var secondsAgo = Int(Date().timeIntervalSince(date))
        if secondsAgo < 0 {
            secondsAgo = secondsAgo * (-1)
        }
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute
        {
            //return "\(secondsAgo) sec ago"
            if secondsAgo < 2{
                return "just now"
            }else{
                return "\(secondsAgo) secs ago"
            }
        }
        else if secondsAgo < hour
        {
            let min = secondsAgo/minute
            if min == 1{
                return "\(min) min ago"
            }else{
                return "\(min) mins ago"
            }
        }
        else if secondsAgo < day
        {
            let hr = secondsAgo/hour
            if hr == 1{
                return "\(hr) hr ago"
            }else{
                return "\(hr) hrs ago"
            }
        }
        else if secondsAgo < week
        {
            let day = secondsAgo/day
            if day == 1{
                return "\(day) day ago"
            }else{
                return "\(day) days ago"
            }

        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, hh:mm a"
            formatter.locale = Locale(identifier: "en_US")
            let strDate: String = formatter.string(from: date)
            return strDate
        }
        
        //return "\(secondsAgo/week) weeks ago"
    }
    
        class func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let now = NSDate()
            let earliest = now.earlierDate(date as Date)
            let latest = (earliest == now as Date) ? date : now
            let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
    
            if (components.year! >= 2) {
                return "\(components.year!) years ago"
            } else if (components.year! >= 1){
                if (numericDates){
                    return "1 year ago"
                } else {
                    return "Last year"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!) months ago"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1 month ago"
                } else {
                    return "Last month"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!) weeks ago"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1 week ago"
                } else {
                    return "Last week"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!) days ago"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1 day ago"
                } else {
                    return "Yesterday"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!) hours ago"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1 hour ago"
                } else {
                    return "An hour ago"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!) minutes ago"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "1 min ago"
                } else {
                    return "A min ago"
                }
            } else if (components.second! >= 3) {
                return "\(components.second!) secs ago"
            } else {
                return "Just now"
            }
    
        }
    
    class func htmlStringFor(str: String)->String
    {
        return str.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    func getImageFromGalleryFromViewController(viewController: UIViewController?, callBack:@escaping (_ info: [String : AnyObject]?, _ selectedImage: UIImage?)-> Void)->Void
    {
        self.currentViewController = viewController
        self.imagePickerHanler = callBack
        let imagePicker: UIImagePickerController = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.mediaTypes = [kUTTypeImage as String]
        
        if viewController != nil
        {
            self.currentViewController = viewController;
            self.currentViewController!.present(imagePicker, animated: true, completion: {
            imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                imagePicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            });
        }
        else
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if ((appDelegate.window?.rootViewController) != nil)
            {
                self.currentViewController = appDelegate.window?.rootViewController!;
                (self.currentViewController)!.present(imagePicker, animated: true, completion: {
                    imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                    imagePicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                });
            }
            else
            {
                self.imagePickerHanler!(nil, nil);
            }
        }
    }
    func getImageFromCameraFromViewController(viewController: UIViewController?,isSelfie:Bool, callBack:@escaping (_ info: [String : AnyObject]?, _ selectedImage: UIImage?)-> Void)->Void
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            self.currentViewController = viewController
            self.imagePickerHanler = callBack
            let imagePicker: UIImagePickerController = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            if isSelfie
            {
                imagePicker.cameraDevice = .front
            }
            
            
            if viewController != nil
            {
                self.currentViewController = viewController;
                self.currentViewController!.present(imagePicker, animated: true, completion: nil);
            }
            else
            {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                if ((appDelegate.window?.rootViewController) != nil)
                {
                    self.currentViewController = appDelegate.window?.rootViewController!;
                    (self.currentViewController)!.present(imagePicker, animated: true, completion: nil);
                }
                else
                {
                    self.imagePickerHanler!(nil, nil);
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
//        let mediaType = info[UIImagePickerControllerMediaType] as? String
//        if mediaType == "public.image"
//        {
//            if let image = info["UIImagePickerControllerEditedImage"] as? UIImage
//            {
//                if self.imagePickerHanler != nil
//                {
//                    self.imagePickerHanler!(info as [String : AnyObject], image);
//                    self.imagePickerHanler  = nil
//                }
//            }
//            else
//            {
//                if self.imagePickerHanler != nil
//                {
//                    self.imagePickerHanler!(nil , nil);
//                    self.imagePickerHanler  = nil
//                }
//            }
//        }
//        self.currentViewController?.dismiss(animated: true, completion: {
//            if !TAHelper.isInternetAvailable()
//            {
//               //  TAAlert.showOkAlertFor(title:"", message:ConstantTextsApi.checkInternetConnection.localizedString, obj: nil, completion: nil)
//            }
//        })
    }
    
    
    class func showSuccessAlert(response: [String: AnyObject], obj: UIViewController)
    {
        TAAlert.showOkAlertFor(title:"", message: response["kMessage"] as! String, obj: obj, completion: nil)
    }
    
    public func showActionSheetFromViewController(viewController: UIViewController, options: [String],title: String, message: String ,onSelect:@escaping (_ selectedIndex: Int, _ selectedTitle: String)-> Void)
    {
        let optionMenu = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: .actionSheet)
        for option in options
        {
            let action = UIAlertAction(title: option, style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                if let index = options.index(of: option), index > -1, index < options.count
                {
                    onSelect(index, option)
                }
                else
                {
                    onSelect(-1, "")
                }
            })
            
            optionMenu.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cancelAction)
        viewController.present(optionMenu, animated: true, completion: nil)
    }
    
    class func customizeTextField(txtBox:UITextField)
    {
        let paddingForFirst = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: txtBox.frame.size.height))
        
        txtBox.leftView = paddingForFirst
        txtBox.leftViewMode = UITextField.ViewMode.always
    }
    

    
    class func getCurrentDate(_ date:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  date// "MMM dd, yyyy"
        let dateString = dateFormatter.string(from:Date())
        print(dateString)
        return dateString
    }
    
    //MARK:- UTC Date Format
    class func utcFormat(dateString : String) -> Date /*String*/
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        let date = formatter.date(from: dateString)
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        let newString = formatter.string(from: date!)
        NSLog("the newString %@", newString)
        let newDate = formatter.date(from: newString)
        print("the date %@", newDate!)
        formatter.dateFormat = "yyyy-MM-dd"
        return newDate!
    }
    
    
    class func getDateStringFromNsdate(_ data : NSDate , _ dateFormt : String)->String
    {
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = dateFormt
        let strDate = dateFormatter.string(from: data as Date)
        return strDate
    }
    
    class func getTimeStringFromNsdate(_ data : NSDate)->String
    {
    let dateFormatter        = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    let strDate = dateFormatter.string(from: data as Date)
    print("The strDate is ---->",strDate)
    return strDate
    }
    
    class func getDesiredFont(fontName : String , fontSize : CGFloat)-> UIFont
    {
        let font = UIFont(name: fontName, size: fontSize)
        return font!
    }
    
}

//MARK: Alert Popups

class TAAlert
{
    
    class func showAlertWithAction(title: String?, message: String?, style: UIAlertController.Style, actionTitles: [String?], action: ((UIAlertAction) -> Void)?) {

        showAlertWithActionWithCancel(title: title, message: message?.lowercased(), style: style, actionTitles: actionTitles, showCancel: false, deleteTitle: nil, action: action)
    }

    class func showAlertWithActionWithCancel(title: String?, message: String?, style: UIAlertController.Style, actionTitles: [String?], showCancel: Bool, deleteTitle: String?, _ viewC: UIViewController? = nil, action: ((UIAlertAction) -> Void)?) {

        let alertController = UIAlertController(title: title, message: message?.lowercased(), preferredStyle: style)
        if deleteTitle != nil {
            let deleteAction = UIAlertAction(title: deleteTitle, style: .destructive, handler: action)
            alertController.addAction(deleteAction)
        }
        for (_, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: action)
            alertController.addAction(action)
        }

        if showCancel {
            let cancelAction = UIAlertAction(title: ConstantTextsApi.cancel.localizedString, style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }
        if let viewController = viewC {

            viewController.present(alertController, animated: true, completion: nil)

        } else {
            let topViewController: UIViewController? = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
            topViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    class func showOkAlertFor(title: String, message: String, obj: UIViewController?, completion: ((_ tag: Int) -> Void)?) -> Void
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertAction.Style.cancel, handler: { (action : UIAlertAction) in
            
            if let _ = completion
            {
                completion!(0)
            }
        }))
        
        if let _ = obj
        {
            obj!.present(alert, animated: true, completion: {})
        }
        else
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlertFor(title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, obj: UIViewController?, completion: @escaping (_ tag: Int) -> Void) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        
        if let _ = obj
        {
            obj!.present(alert, animated: true, completion: {})
        }
        else
        {
            let topViewController: UIViewController? = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
            topViewController?.present(alert, animated: true, completion: nil)
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            TAHelper.performTaskInMainQueue {
//                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
//            }
            
        }
    }
    
    class func showAlertOnActiveC(title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, completion: @escaping (_ tag: Int) -> Void) {
        
        guard let activeVieC = TAHelper.topMostController()
            else
        {
            return
        }
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        activeVieC.present(alert, animated: true, completion: {})
    }
    
}

// Cache Directory
extension TAHelper
{
    class func cachePathForFile() -> String
    {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        let CacheDirectoryURL = urls[urls.count-1] as URL
        let dbDirectoryURL = CacheDirectoryURL.appendingPathComponent("CacheFile")
        
        if FileManager.default.fileExists(atPath: dbDirectoryURL.path) == false
        {
            do{
                try FileManager.default.createDirectory(at: dbDirectoryURL, withIntermediateDirectories: false, attributes: nil)
            }catch{
            }
        }
        return dbDirectoryURL.absoluteString
    }
    
    class func writeFileInCacheFor(data: Data, fileName: String) -> URL?
    {
        let docsDir: String = TAHelper.cachePathForFile()
        if docsDir != ""
        {
            let filePath: String = "\(docsDir)/\(fileName)"
            let url = (URL(string: filePath))!
            do {
                try data.write(to: url, options: .atomic)
                return url
            } catch {
                TADebug.Log(message: error)
            }
        }
        return nil
    }
    
    class func isCacheFileExitAtPath(filePath: String) -> Bool
    {
        let fileurl: String = "\(TAHelper.cachePathForFile())/\(filePath)"
        let isExist: Bool = FileManager.default.fileExists(atPath: fileurl)
        return isExist
    }
    
    class func removeAllCacheMusics()
    {
        let fileManager = FileManager.default
        let cachePath = TAHelper.cachePathForFile()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: cachePath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: cachePath + filePath)
            }
        } catch {
            TADebug.Log(message: "Could not clear Cache Musics: \(error)")
        }
    }
}


// Application File Document Directory and Path
extension TAHelper
{
    class func pathForMusicDir() -> String
    {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count-1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("MyMusic")
        
        if FileManager.default.fileExists(atPath: dbDirectoryURL.path) == false
        {
            do{
                try FileManager.default.createDirectory(at: dbDirectoryURL, withIntermediateDirectories: false, attributes: nil)
            }catch{
            }
        }
        return dbDirectoryURL.absoluteString
    }
    
    class func pathToFilesDir() -> String
    {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count-1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("Files")
        
        if FileManager.default.fileExists(atPath: dbDirectoryURL.path) == false
        {
            do{
                try FileManager.default.createDirectory(at: dbDirectoryURL, withIntermediateDirectories: false, attributes: nil)
            }catch{
            }
        }
        return dbDirectoryURL.absoluteString
    }
    
    class func writeFileFor(data: Data, fileName: String) -> URL?
    {
        let docsDir: String = TAHelper.pathToFilesDir()
        if docsDir != ""
        {
            let filePath: String = "\(docsDir)/\(fileName)"
            let url = (URL(string: filePath))! //URL(fileURLWithPath: filePath)
            do {
                try data.write(to: url, options: .atomic)
                return url
            } catch {
                TADebug.Log(message: error)
            }
        }
        return nil
    }
    
    class func imageAtPath(filePath: String) -> UIImage?
    {
        if filePath == "" {
            return nil
        }
        
        let fileurl: String = "\(TAHelper.pathToFilesDir())/\(filePath)"
        var image: UIImage? = nil
        let isExist: Bool = FileManager.default.fileExists(atPath: fileurl)
        if isExist == false {
            return nil
        }
        let url = URL(fileURLWithPath: fileurl)
        var data: Data?
        do {
            data = try Data(contentsOf: url)
        } catch {
        }
        
        if data != nil {
            image = UIImage(data: data!)
        }
        return image!
    }
    
    class func isMusicFileExitAtPath(filePath: String) -> Bool
    {
        let url = URL(string: filePath)
        if url != nil {
            let isExist: Bool = FileManager.default.fileExists(atPath: (url?.path)!)
            return isExist
        }
        
        return false
    }
    
    class func deleteFileFromDir(url: URL) -> Bool
    {
        var isDeleted: Bool = false
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: url.path) {
            if ((try? fileManager.removeItem(at: url)) != nil) {
                isDeleted = true
            }
            else {
                isDeleted = false
            }
        }
        return isDeleted
    }
    
    class func uniqueFileNameWithExtention(fileExtension: String) -> String
    {
        let uniqueString: String = ProcessInfo.processInfo.globallyUniqueString
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmsss"
        let dateString: String = formatter.string(from: Date())
        let uniqueName: String = "\(uniqueString)_\(dateString)"
        if fileExtension.length > 0 {
            let fileName: String = "\(uniqueName).\(fileExtension)"
            return fileName
        }
        
        return uniqueName
    }
    
    class func fileSize(forURL url: Any) -> Double
    {
        var fileURL: URL?
        var fileSize: Double = 0.0
        if (url is URL) || (url is String)
        {
            if (url is URL) {
                fileURL = url as? URL
            }
            else {
                fileURL = URL(fileURLWithPath: url as! String)
            }
            var fileSizeValue = 0.0
            try? fileSizeValue = (fileURL?.resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Double?)!
            if fileSizeValue > 0.0 {
                fileSize = (Double(fileSizeValue) / (1024 * 1024))
            }
        }
        return fileSize
    }
    
    
    class func stringSectionDate(timeStamp: TimeInterval) -> String
    {
        
        let date = Date(timeIntervalSince1970: timeStamp)
        if NSCalendar.current.isDateInToday(date)
        {
            return "Today"
        }
        else if NSCalendar.current.isDateInYesterday(date)
        {
            return "Yesterday"
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        formatter.locale = Locale(identifier: "en_US")
        let strDate: String = formatter.string(from: date)
        return strDate
    }

    class func stringSectionDateChatList(timeStamp: TimeInterval) -> String
    {
        
        let date = Date(timeIntervalSince1970: timeStamp)
        if NSCalendar.current.isDateInToday(date)
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let str = formatter.string(from: date)
            return str
        }
        else if NSCalendar.current.isDateInYesterday(date)
        {
            return "Yesterday"
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM, yy"
        formatter.locale = Locale(identifier: "en_US")
        
        let strDate: String = formatter.string(from: date)
        return strDate
    }
    
    
    class func activityIndicator(view:UIView) {
        removeActivityIndicator(view: view)
        view.isUserInteractionEnabled = false
        let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        var activityIndicator = UIActivityIndicatorView()
        effectView.frame = CGRect(x:UIScreen.main.bounds.midX - 23 , y: UIScreen.main.bounds.midY - 23 , width: 46, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        effectView.tag = 11100
//        activityIndicator = UIActivityIndicatorView(style: .UIActivityIndicatorView.Style.medium)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        effectView.contentView.addSubview(activityIndicator)
        view.addSubview(effectView)
    }
    class func removeActivityIndicator(view:UIView) {
        view.isUserInteractionEnabled = true
        if let subview = view.viewWithTag(11100) as? UIVisualEffectView{
            subview.removeFromSuperview()
        }
    }
}


// MARK: - Done Button On KeyBoard
extension UITextField {
    func addDoneButtonOnKeyBoardWithControl() {
        let kTextColor = UIColor(red: 55.0/255.0, green: 70.0/255.0, blue: 126.0/255.0, alpha: 1.0)
        let dateToolBar = UIToolbar()
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(UIView.endEditing(_:)))
        dateToolBar.barStyle = UIBarStyle.default
        dateToolBar.isTranslucent = true
        dateToolBar.tintColor = kTextColor
        dateToolBar.sizeToFit()
        dateToolBar.setItems([flex,doneButtonDate], animated: false)
        dateToolBar.isUserInteractionEnabled = true
        self.inputAccessoryView = dateToolBar
    }
}


//MARK: TADebug

let isDebugModeOn = true

class TADebug {
    
    class func Log<T>(message: T, functionName: String = #function, fileNameWithPath: String = #file, lineNumber: Int = #line ) {
        
        TAThreads.performTaskInBackground {
            if isDebugModeOn {
                let fileNameWithoutPath:String = NSURL(fileURLWithPath: fileNameWithPath).lastPathComponent ?? ""
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm:ss.SSS"
                let output = "\r\n❗️\(fileNameWithoutPath) => \(functionName) (line \(lineNumber), at \(dateFormatter.string(from: NSDate() as Date)))\r\n => \(message)\r\n"
                print(output)
            }
        }
    }
}



