//
//  CustomButton.swift
//  Venue
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Techahead Softwares. All rights reserved.


import UIKit

class CustomButton: UIButton {
    
    var strSessionId:String?
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        let screenRect:CGRect           = UIScreen.main.bounds
        let screenWidth:CGFloat         = screenRect.size.width
        let scalefactor:CGFloat         = screenWidth / 375.0
        self.titleLabel!.font           =  UIFont(name: (self.titleLabel!.font.fontName), size: (self.titleLabel!.font.pointSize)*scalefactor)!
        self.isExclusiveTouch           = true
    }
    
    
}
