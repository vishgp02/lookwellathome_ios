//
//  UIColor+Additions.swift
//  OneNation
//
//  Created by Ashish Chauhan on 10/31/17.
//  Copyright © 2017 Techahead Software. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var trendingNews: UIColor {
        return  UIColor.colorWith(hexString: "#f73b48")
    }
    
    class var borderColorBlack: UIColor {
        return  UIColor(red: 2.0/255.0, green: 2.0/255.0, blue: 2.0/255.0, alpha: 0.2)
    }
    
    class var borderColorLightGrey: UIColor {
        return  UIColor(red: 210.0/255.0, green: 208.0/255.0, blue: 208.0/255.0, alpha: 0.5)
    }
    
    class var backgroundColor: UIColor {
        return  UIColor(red: 100.0/255.0, green: 188.0/255.0, blue: 187.0/255.0, alpha: 1.0)
    }
    
    class var blackTextColor: UIColor {
        return  UIColor(red: 66.0/255.0, green: 62.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    }
    
    class var storeBgcolor: UIColor{
        return  UIColor(red: 71.0/255.0, green: 168.0/255.0, blue: 169.0/255.0, alpha: 1.0)
    }
    
    class var moreTabBgColor: UIColor {
        return  UIColor(red: 178.0/255.0, green: 168.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    }
    
//    class var indicatorColor: UIColor {
//        return  UIColor.colorWith(hexString:EventTypeColor.WarmGrey.description)
//    }
    
    class var accountsBarBGColor: UIColor{
        return  UIColor(red: 245.4/255.0, green: 245.4/255.0, blue: 245.4/255.0, alpha: 1.0)
    }
    
    class var ratingViewBorderColor: UIColor {
        return UIColor.colorWithRGBA(redC: 71.0, greenC: 169.0, blueC: 168.0, alfa: 1.0)
    }
    
    class var languageCellBorderColor: UIColor{
      return UIColor.colorWithRGBA(redC: 198.0, greenC: 197.0, blueC: 197.0, alfa: 1.0)
    }
    
    class var genderUnselectedColor: UIColor {
       return UIColor.colorWithRGBA(redC: 221.0, greenC: 223.0, blueC: 222.0, alfa: 1.0)
    }
    
    // MARK: UIColor additional properties
    class func colorWith(hexString:String)->UIColor {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.utf16.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func colorWithRGBA(redC:CGFloat, greenC:CGFloat, blueC:CGFloat, alfa:CGFloat) -> UIColor {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = alfa
        red   = CGFloat(redC/255.0)
        green = CGFloat(greenC/255.0)
        blue  = CGFloat(blueC/255.0)
        alpha  = CGFloat(alpha)
        let color: UIColor =  UIColor(red:CGFloat(red), green: CGFloat(green), blue:CGFloat(blue), alpha: alpha)
        return color
    }
    
    class func colorWithAlpha(color:CGFloat, alfa:CGFloat) -> UIColor {
        let red:   CGFloat = CGFloat(color / 255.0)
        let green: CGFloat = CGFloat(color / 255.0)
        let blue:  CGFloat = CGFloat(color / 255.0)
        let alpha: CGFloat = alfa
        let color: UIColor =  UIColor(red:red, green:green, blue:blue, alpha:alpha)
        return color
    }
}
