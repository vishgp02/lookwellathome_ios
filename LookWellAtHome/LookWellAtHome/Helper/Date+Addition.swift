//
//  Date+Addition.swift
//  TheDon
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Ashish Chauhan. All rights reserved.
//

import UIKit

extension Date {
    
    
    public func removeTimeStamp() -> Int64 {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: self)) else {
            fatalError("Failed to strip time from Date object")
        }
        return Int64(date.timeIntervalSince1970)
    }
    
//    var startOfWeek: Date? {
//        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
//    }
    
    func localDate() -> Date {
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
    }
    // or GMT time
    func utcDate() -> Date {
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = -TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
   }
    
    func getStringFromDate(_ date:Date,format:String) -> String?
    {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        let strDate = formatter.string(from:date)
        
        return strDate
    }
    
    func getDateFromString(_ strDate:String,format:String)-> Date?
    {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
       let date = formatter.date(from:strDate)
        
       return date
    }
    
    func getTimeStampFrom(datestr: String,format:String) -> Int?
    {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        //formatter.timeZone = TimeZone.current
        formatter.timeZone = TimeZone.init(identifier:"UTC")
        formatter.dateFormat = format
        if let date = formatter.date(from: datestr){
            let timestamp = date.timeIntervalSince1970
            return Int(timestamp)
        }
        
        return nil
        
    }
    
    func getDateStr(_ dateStr: String,_ dateFormater:String,_ convertFormat:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormater
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        if let date = formatter.date(from: dateStr) {
            let newFormatter = DateFormatter()
            newFormatter.dateFormat = convertFormat
            newFormatter.timeZone = TimeZone.current
            let dateStrFinal = newFormatter.string(from: date)
            return dateStrFinal
        }
        return ""
    }
    
     func getDateFrom(timeSatmp:Double) -> Date {
        let date = Date(timeIntervalSince1970:timeSatmp)
        return date
    }
    
//    func toGetDateFifteenDayAheadOfCurrentDate() -> [Date]? {
//        var arrDate:[Date]? = []
//        let date = Date().startOfWeek
//        for i in 0...13{
//            let date = NSCalendar.current.date(byAdding:.day, value: i, to: date!, wrappingComponents:true)
//            arrDate?.append(date!)
//        }
//        return arrDate
//    }
    
    func getRemainingDateFrom(_ date:Date) -> Int? {
        let date1 = NSCalendar.current.startOfDay(for: date)
        let date2 = NSCalendar.current.startOfDay(for: Date())
        let components = NSCalendar.current.dateComponents([.day], from: date1, to: date2)
        if let day = components.day {
           return abs(day)
        }
        return 0
    }
    
    static func getNotificationTimeFromtime(fromString strdate:String?,inFormat:String) -> String?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        // dateFormatter.setLocalizedDateFormatFromTemplate(inFormat)
        
        let date = dateFormatter.date(from:strdate!)
        
        //let localDate = self.utcDateToLocalDate(dateUTC:date)
        
        if let _ = date
        {
            var interval: Int = Int(Date().timeIntervalSince(date!))
            
            interval = labs(interval)
            
            if interval < 60
            {
                if interval <= 10
                {
                    return "Just Now"
                }
                return "\(Int(interval))" + " sec ago"
            }
            else if interval >= 60 && interval < 60 * 60
            {
                if interval / 60 == 1
                {
                    return "\(Int(interval) / 60)" + " min ago"
                }
                return "\(Int(interval) / 60)" + " min ago"
            }
            else if interval >= 60 * 60 && interval < 60 * 60 * 24
            {
                if (interval / 60) / 60 == 1
                {
                    return "\(Int(interval / 60) / 60)" + " hr ago"
                }
                return "\(Int(interval / 60) / 60)" + " hrs ago"
            }
            else
            {
                if ((interval / 60) / 60) / 24 == 1
                {
                    return "\(Int((interval / 60) / 60) / 24)" + " day ago"
                }
                return "\(Int((interval / 60) / 60) / 24)" + " days ago"
            }
        }
        
        return ""
    }
}
