//
//  ImgPickerHandler.swift
//  CRIIIO
//
//  Created by Jobin John on 22/10/18.
//  Copyright © 2018 TechAhead. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
//import AssetsPickerViewController
import Photos
import AVKit


protocol ImgPickerHandlerDelegate: class {
    func dismissView()
}

enum AttachmentType: String{
      case camera, video, photoLibrary
  }

//MARK: - Constants
struct Constants {
    static let actionFileTypeHeading = "Add a File"
    static let actionFileTypeDescription = "Choose a filetype to add..."
    static let camera = "Camera"
    static let phoneLibrary = "Phone Library"
    static let video = "Video"
    static let file = "File"
    
    
    static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
    
    static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
    
    static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
    
    
    static let settingsBtnTitle = "Settings"
    static let cancelBtnTitle = "Cancel"
    
}
class ImgPickerHandler: NSObject {

    enum PermissionType {
        case camera, photo
        var alertMessage: String {
            switch self {
            case .camera:
                return "app does not have access to your camera, go to settings and turn on camera."
            case . photo:
                return "app does not have access to your photos, go to settings and turn on photo library access."
            }
        }
    }
    fileprivate var currentVC: UIViewController?

    let KMAXMediaSelectionLimit = 5
    let KMAXVideoDurationLimitMinute = 1
    let KMAXImageSelectionSizeLimitInMB = 10

    lazy var KMAXVideoDurationLimit: Double  = { return Double(KMAXVideoDurationLimitMinute * 60) }()  //1 min
    lazy var KMAXImageSelectionSizeLimitInBytes: Int = {return 10 * 1024 * 1024  }()  // 10 mb

    static let sharedHandler = ImgPickerHandler()
    var guestInstance: UIViewController?
    var imageClosure: ((UIImage)->Void)?
    var imgInfoClosure: (([String: Any])->Void)?
    var videoClosure: ((URL)->Void)?
    var imageData: (([Data?])->Void)?
    var selectedPHAssets: (([PHAsset])->Void)?
    var cameraPHAssets: ((PHAsset)->Void)?
    weak var delegate:ImgPickerHandlerDelegate?
    
    // Public Methods
    func getImage(instance: UIViewController, completion: ((_ myImage: UIImage)->Void)?) {
        
        let actionSheet = UIAlertController(title: "Choose", message: nil, preferredStyle: .actionSheet)     //Select to upload
             
          guestInstance = instance
          let imgPicker = UIImagePickerController()
          imgPicker.delegate = self
          imgPicker.allowsEditing = true
          imgPicker.mediaTypes = [(kUTTypeImage) as String]
        
        let actionSelectCamera = UIAlertAction(title: "Camera", style: .default, handler: {
            _ in
             self.openCamera(picker: imgPicker)

        })
        
        let actionSelectGallery = UIAlertAction(title: "Gallery", style: .default, handler: {
                   _ in
                    self.openGallery(picker: imgPicker)

               })
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(actionCancel)
        actionSheet.addAction(actionSelectCamera)
        actionSheet.addAction(actionSelectGallery)

        
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        }  
          imageClosure = {
              (image) in
              completion?(image)
          }
        
      }

    func getImageDict(instance: UIViewController, rect: CGRect?, completion: ((_ imgDict: [String: Any])->Void)?) {
        guestInstance = instance
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.allowsEditing = true
        imgPicker.mediaTypes = [(kUTTypeImage) as String]
        let actionSheet = UIAlertController(title: "Choose", message: nil, preferredStyle: .actionSheet)
        let actionSelectCamera = UIAlertAction(title: "Camera", style: .default, handler: {
            _ in
            self.openCamera(picker: imgPicker)
        })
        let actionSelectGallery = UIAlertAction(title: "Gallery", style: .default, handler: {
            _ in
            self.openGallery(picker: imgPicker)

        })
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(actionCancel)
        actionSheet.addAction(actionSelectCamera)
        actionSheet.addAction(actionSelectGallery)

        if UIDevice.current.userInterfaceIdiom == .phone {
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        } else {
            actionSheet.popoverPresentationController?.sourceView = guestInstance?.view
            actionSheet.popoverPresentationController?.sourceRect = rect!
            actionSheet.popoverPresentationController?.permittedArrowDirections = .up
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        }
        imgInfoClosure = {
            dictInfo in
            completion?(dictInfo)
        }
    }


    func getImageDict(instance: UIViewController, isSourceCamera: Bool, rect: CGRect?, completion: ((_ myImage: UIImage)->Void)?) {
        guestInstance = instance
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.allowsEditing = true
        imgPicker.mediaTypes = [(kUTTypeImage) as String]
        if isSourceCamera {
            self.openCamera(picker: imgPicker)
        } else {
            self.openGallery(picker: imgPicker)
        }
         imageClosure = {
                    (image) in
                    completion?(image)
                }
    }

//    func getVideo(instance: UIViewController, isSourceCamera: Bool, completion: ((_ myVideoUrl: URL)->Void)?) {
//        guestInstance = instance
//        let imgPicker = UIImagePickerController()
//        imgPicker.delegate = self
//        imgPicker.allowsEditing = true
//        imgPicker.mediaTypes = [(kUTTypeMovie) as String]
//        imgPicker.videoMaximumDuration = TimeInterval(Config.kVideoMaxDuration*60*60)
//        imgPicker.videoQuality = UIImagePickerController.QualityType.typeIFrame960x540
//        if isSourceCamera {
//            self.openCamera(picker: imgPicker)
//        } else {
//            self.openGallery(picker: imgPicker)
//        }
//        videoClosure = {
//            (url) in
//            completion?(url)
//        }
//    }

    //Private Methods
    private func openCamera(picker: UIImagePickerController) {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {

            picker.sourceType = UIImagePickerController.SourceType.camera
        //    picker.cameraDevice = UIImagePickerController.CameraDevice.front   // front camera by default
            self.checkAndAskForCameraPermission { (cameraPermissionStatus) in
                if cameraPermissionStatus {
                    TAUtility.performTaskInMainQueue { [weak self] in
                        guard let wkSelf = self else { return }
                        wkSelf.guestInstance?.present(picker, animated: true, completion: nil)
                    }

                }
            }
        } else {
            let alert = UIAlertController(title: "Warning", message: "Camera not available", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(actionOK)
            TAUtility.performTaskInMainQueue { [weak self] in
                guard let wkSelf = self else { return }
                wkSelf.guestInstance?.present(alert, animated: true, completion: nil)
            }

        }
    }

    private func openGallery(picker: UIImagePickerController) {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.checkAndAskForPhotosPermission(completion: { (photosPermissionStatus) in
            if photosPermissionStatus {
                TAUtility.performTaskInMainQueue { [weak self] in
                    guard let wkSelf = self else { return }
                    wkSelf.guestInstance?.present(picker, animated: true, completion: nil)
                }
            }
        })
    }

    func getAssetData(asset: PHAsset, completion: ((Data?) -> Void)?) {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.resizeMode = .exact
        option.isSynchronous = true
        option.isNetworkAccessAllowed = true
        manager.requestImageData(for: asset, options: option) { (data, _, _, _) in
            if let completion = completion {
                completion(data)
            }
        }
//        manager.requestImage(for: asset, targetSize: CGSize(width: 1000.0, height: 1000.0), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
//            thumbnail = result!
//        })

    }

    func checkAndAskForCameraPermission(completion: @escaping ((Bool)->Void)) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            completion(true)
        } else if AVCaptureDevice.authorizationStatus(for: .video) ==  .restricted || AVCaptureDevice.authorizationStatus(for: .video) ==  .denied {
            self.moveToAppSettingsWithAlert(type: .camera)
            completion(false)
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    completion(true)
                } else {
                    self.moveToAppSettingsWithAlert(type: .camera)
                    completion(false)
                }
            })
        }
    }

    func checkAndAskForPhotosPermission(completion: @escaping ((Bool)->Void)) {
        if PHPhotoLibrary.authorizationStatus() ==  .authorized {
            completion(true)
        } else if PHPhotoLibrary.authorizationStatus() ==  .restricted || PHPhotoLibrary.authorizationStatus() ==  .denied {
            self.moveToAppSettingsWithAlert(type: .photo)
            completion(false)
        } else {
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized {
                    completion(true)
                } else {
                    self.moveToAppSettingsWithAlert(type: .photo)
                    completion(false)
                }
            })
        }
    }

    func moveToAppSettingsWithAlert(type: PermissionType) {
        
        TAUtility.performTaskInMainQueue { [weak self] in
            guard let wkSelf = self else { return }
            TAAlert.showAlertWithAction(title: ConstantTextsApi.AppName.rawValue, message: type.alertMessage, style: .alert, actionTitles: ["cancel", "settings"], action: { (action) in
                if action.title == "settings" {
                    let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            })
        }
    }

    
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        TAUtility.performTaskInMainQueue { [weak self] in
            guard let wkSelf = self else { return }
            wkSelf.currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
        }
    }

    
    func authorisationStatusAtAppdelegate(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                //openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                //photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                //videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        //self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        //self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        //self.videoLibrary()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
}

// MARK: - UIImagePicker Delegates
extension ImgPickerHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //Cancel button  of imagePicker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if self.delegate != nil {
            self.delegate?.dismissView()
        }
        picker.dismiss(animated: true, completion: nil)
    }

    //Picking Action of ImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
//        imgInfoClosure?(info.description)
        //UIImagePickerControllerOriginalImage
        if let img = info[.originalImage] as? UIImage {
            imageClosure?(img)
            
//            func fetchImage() {
//                PHPhotoLibrary.shared().performChanges({
//                    PHAssetChangeRequest.creationRequestForAsset(from: img)
//                }) { saved, _ in
//                    if saved {
//                        let fetchOption = PHFetchOptions()
//                        fetchOption.includeHiddenAssets = true
//                        fetchOption.includeAllBurstAssets = true
//                        fetchOption.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
//                        if let asset = PHAsset.fetchAssets(with: .image, options: fetchOption).lastObject {
//
//                            if let cameraPHAsset = self.cameraPHAssets {
//                                cameraPHAsset(asset)
//                            }
//
////                              cameraPHAssets self?.processCameraImage(asset: assets)
//                        }
//                    } else {
////                        Logs.debug(error)
//                    }
//                }
//            }
            if PHPhotoLibrary.authorizationStatus() == .authorized {
                //fetchImage()
            } else {
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized {
                       // fetchImage()
                    }
                })
            }

        } else if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            videoClosure?(videoUrl)
            func fetchVideo() {
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)
                }) { saved, _ in
                    if saved {
                        let fetchOptions = PHFetchOptions()
                        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]

                        // After uploading we fetch the PHAsset for most recent video and then get its current location url

                        if let asset = PHAsset.fetchAssets(with: .video, options: fetchOptions).lastObject {
                            if let cameraPHAsset = self.cameraPHAssets {
                                cameraPHAsset(asset)
                            }
                        }
                    }
                }
            }

            if PHPhotoLibrary.authorizationStatus() == .authorized {
                fetchVideo()
            } else {
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized {
                        fetchVideo()
                    }
                })
            }
        }
    }
}


