//
//  TADefine.swift
//  HealthyMummy
//
//  Created by Shashank Gupta on 16/11/16.
//  Copyright © 2016 TechAhead. All rights reserved.
//

import Foundation
import UIKit

class TADefines: NSObject {
}

#if DEBUG
func DLog(message: String, filename: String = #file, function: String = #function, line: Int = #line) {
    print("[\((filename as NSString).lastPathComponent):\(line)] \(function) - \(message)")
}

func classStart(filename: String = #file, function: String = #function, line: Int = #line) {
    print("ClassStart", "[\((filename as NSString).lastPathComponent):\(line)] \(function)")
}

func classEnd(filename: String = #file, function: String = #function, line: Int = #line) {
    print("ClassEnd", "[\((filename as NSString).lastPathComponent):\(line)] \(function)")
}

func ALog(message: String, filename: String = #file, function: String = #function, line: Int = #line) {
    let alertView = UIAlertView(title: "[\((filename as NSString).lastPathComponent):\(line)]", message: "\(function) - \(message)", delegate: nil, cancelButtonTitle: "OK")
    alertView.show()
}

func FLog(filename: String = #file, function: String = #function, line: Int = #line) {
    print("FlowAction", "[\((filename as NSString).lastPathComponent):\(line)] \(function)")
}

#else
func DLog(message: String, filename: String = #file, function: String = #function, line: Int = #line) {
}

func classStart(filename: String = #file, function: String = #function, line: Int = #line) {
}

func classEnd(filename: String = #file, function: String = #function, line: Int = #line) {
}

func ALog(message: String, filename: String = #file, function: String = #function, line: Int = #line) {
}

func FLog(filename: String = #file, function: String = #function, line: Int = #line) {
}

#endif

/**
 * Macros
 */

// -- temp variable --

let ISIPAD                   = UIDevice.current.userInterfaceIdiom == .pad
let ISIPHONE                 = UIDevice.current.userInterfaceIdiom == .phone
let ISUNSP                   = UIDevice.current.userInterfaceIdiom == .unspecified

let ISIPHONE4                = UIScreen.main.bounds.size.height == 480.0 ? true: false
let ISIPHONE5                = UIScreen.main.bounds.size.height == 568.0 ? true: false
let ISIPHONE6                = UIScreen.main.bounds.size.height == 667.0 ? true: false
let ISIPHONE6PLUS            = UIScreen.main.bounds.size.height == 736.0 ? true: false

let ISIPHONEX                = UIScreen.main.nativeBounds.height == 2436 ? true: false
let ISIPHONEXR               = UIScreen.main.nativeBounds.height == 1792 ? true: false
let ISIPHONEXS               = UIScreen.main.nativeBounds.height == 2436 ? true: false
let ISIPHONEXSMax            = UIScreen.main.nativeBounds.height == 2688 ? true: false

let kAppDelegate             = UIApplication.shared.delegate as? AppDelegate
@available(iOS 13.0, *)
let kSceneDelegate             = UIApplication.shared.delegate as? SceneDelegate


let kDeviceSize              = UIScreen.main.bounds.size
let kDeviceWidth             = kDeviceSize.width
let kDeviceHeight            = kDeviceSize.height
let kUserDefaults            = UserDefaults.standard
let kTabbarTintColor         = UIColor(red: 40/255.0, green: 176/255.0, blue: 252/255.0, alpha: 1.0)
let kThemeColor              = UIColor(red: 250.0/255.0, green: 81.0/255.0, blue: 128.0/255.0, alpha: 1.0)
let kRedColor                = UIColor(red: 230/255, green: 6/255, blue: 6/255, alpha: 1.0)

let kDefaultBlueColor = "#02AFF2"
let kDefaultBlackColor = "#242425"

//let StatusbarDefaultHeight: CGFloat = TAUtility.isIphoneX() ? 44.0 : 20.0
let NavigationBarDefaultHeight: CGFloat = 44.0
let ToolBarDefaultHeight: CGFloat = 44.0
let TabBarDefaultHeight: CGFloat = 49.0

// UIFonts

var kAppName                                = "ToothFARE"
let kConstAppVersion                        = "1.0"
let kConstPushType                          = "1" //Dev-1, Dis-0

let kHexColorAppTheme                       = "#1AAF54"
let kGoogleClientId                         = ""
let kBlobUrl = "https://toothblob.blob.core.windows.net/toothfarecontainer/"
/**
 * Storyboard Name
 */
let kStoryboardLogin        =  UIStoryboard(name: "Login", bundle: nil)
let kStoryboardMain        =  UIStoryboard(name: "Main", bundle: nil)
let kStoryboardHomeTabbar        =  UIStoryboard(name: "HomeTabBar", bundle: nil)
let kStoryboardHome        =  UIStoryboard(name: "Home", bundle: nil)
let kStoryboardChildren        =  UIStoryboard(name: "Children", bundle: nil)
let kStoryboardMessage        =  UIStoryboard(name: "Message", bundle: nil)
let kStoryboardProfile       =  UIStoryboard(name: "Profile", bundle: nil)
let kStoryboardFaq        =  UIStoryboard(name: "Faq", bundle: nil)


//PlaceHolder Texts

let kMsgNoInternetConnection                    = "Oops! Something went wrong. Please check your internet connection."
//"There seems to be a problem. Please check your internet connection."
let kMsgServiceNotResponding                    = "There seems to be a problem. Please try again a little later."

let kMsgUnableToFindLocation                    = "Unable to find current location"
let kMsgNoDataFound                             = "No data found"
let kMsgEnterEmail                              = "Please enter Email"
let kMsgEnterEmailUsername                      = "Please enter Email or Username"
let kMsgEnterValidEmail                         = "Please enter valid Email"
let kMsgEnterUserName                           = "Please enter UserName"
let kMsgEnterValidUserName                      = "UserName must be 6 character long"
let kMsgEnterEmailPhone                      = "Please enter email or phone number"

let kMsgAgreeTos                                = "Please agree to our Terms & Conditions and Privacy Policy"
let kMsgEnterUsername                           = "Please enter username"
let kMsgEnterFullName                           = "Please enter full name"
let kMsgEnterMessage                            = "Please enter your message"
let kMsgEnterLabel                              = "Please enter Label"
let kMsgEnterDesc                               = "Please enter Description"
let kMsgEnterValidLink                          = "Please enter valid link"

let kMsgEnterName                               = "Please enter Name"
let kMsgEnterFirstName                          = "Please enter First Name"
let kMsgEnterLastName                           = "Please enter Last Name"
let kMsgEnterMobileNo                           = "Please enter Mobile Number"
let kMsgEnterPhoneNo                           = "Please enter Phone Number"
let kMsgEnterValidPhoneNo                            = "Please enter valid Phone Number"
let kMsgMobileNoLength                          = "Please enter a valid phone number of 8-13 numbers."
let kMsgEnterCountryCode                        = "Please enter Country Code"
let kMsgCountryCodeLength                       = "Please select country for country code"//"Country Code must be between 2-4 digit long"
let kPleaseWait                                 = "Please wait"
let kDeviceType                                 = "0"
let kLoginType                                  = "0"
//Device Type (0: Manual, 1: Facebook, 2: Instagram, 3: Twitter, 4: Google)

/**
 * Font Name
 */

struct Fonts {
    static let RobotoRegular             = "Roboto-Regular"
    static let RobotoBlack               = "Roboto-Black"
    static let RobotoLight               = "Roboto-Light"
    static let RobotoBoldItalic          = "Roboto-BoldItalic"
    static let RobotoLightItalic         = "Roboto-LightItalic"
    static let RobotoThin                = "Roboto-Thin"
    static let RobotoMediumItalic        = "Roboto-MediumItalic"
    static let RobotoMedium              = "Roboto-Medium"
    static let RobotoBold                = "Roboto-Bold"
    static let RobotoBlackItalic         = "Roboto-BlackItalic"
    static let RobotoItalic              = "Roboto-Italic"
    static let RobotoThinItalic          = "Roboto-ThinItalic"
}




struct ColorConstants {
//    static let pink = UIColor.colorWithRGBA(redC: 244, greenC: 83, blueC: 129, alfa: 1.0)
//    static let green = UIColor.colorWithRGBA(redC: 100, greenC: 181, blueC: 182, alfa: 1.0)
//    static let graphGreen = UIColor.colorWithRGBA(redC: 12, greenC: 185, blueC: 186, alfa: 1.0)
//    static let textGreen = UIColor.colorWithRGBA(redC: 73, greenC: 197, blueC: 207, alfa: 1.0)
//    static let graphLightBlue = UIColor.colorWithRGBA(redC: 118, greenC: 214, blueC: 245, alfa: 1.0)
//    static let graphOrange = UIColor.colorWithRGBA(redC: 243, greenC: 158, blueC: 71, alfa: 1.0)
//    static let graphPurple = UIColor.colorWithRGBA(redC: 129, greenC: 91, blueC: 210, alfa: 1.0)
//    static let color153 = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
//    static let timerLightPurple = UIColor.colorWithRGBA(redC: 186, greenC: 104, blueC: 200, alfa: 1.0)
//    static let timerPurple = UIColor.colorWithRGBA(redC: 128, greenC: 0, blueC: 128, alfa: 1.0)
//    static let timerGreen = UIColor.colorWithRGBA(redC: 28, greenC: 207, blueC: 49, alfa: 1.0)
//    static let onboardingContinueButtonColor = UIColor(red: 244.0/255.0, green: 71.0/255.0, blue: 126.0/255.0, alpha: 1.0)
////    static let onboardingSpotlightMaskColor = onboardingContinueButtonColor.withAlphaComponent(0.2)
//    static let onboardingSpotlightMaskColor = UIColor.clear
}

// MARK: - Device -
struct Device {
    static let bounds               = UIScreen.main.bounds
    static let width                = UIScreen.main.bounds.size.width
    static let height               = UIScreen.main.bounds.size.height
    static let isIPad               = UIDevice.current.userInterfaceIdiom == .pad
    static let isIPhone             = UIDevice.current.userInterfaceIdiom == .phone
    static let isUnspecified        = UIDevice.current.userInterfaceIdiom == .unspecified
    
    static let isIphone4            = UIScreen.main.bounds.size.height == 480.0 ? true: false
    static let isIphone5            = UIScreen.main.bounds.size.height == 568.0 ? true: false
    static let isIphone6            = UIScreen.main.bounds.size.height == 667.0 ? true: false
    static let isIphone6Plus        = UIScreen.main.bounds.size.height == 736.0 ? true: false
}

struct GeneralConstants {
    static let cornerRadius                 = CGFloat(4)
    static let validNameLength              = 60
    static let validEmailLength             = 60
    static let maxZipCodeLength             = 5
    static let minPhoneLength               = 8
    static let maxPhoneLength               = 13
    static let minPasswordLength            = 6
    static let maxPasswordLength            = 12
    static let validSupportTextLength       = 450
    static let supportEmailId               = ""
    
    //    static let keychainPasswordKey = "group.com.ta.store"
    static let deviceType          = "1" // for iOS
    
    static let appName             = "ToothFARE"
    static let defaultDateFormat   = "d MMMM YYYY"
    static let MMMddDateFormat     = "dd/MM/yy"//"MMM dd"
    static let MMddyyDateFormat    = "dd/MM/yy"
    static let name                = "name"
    static let value               = "value"
    static let valueKey            = "valueKey"
    static let key                 = "key"
    static let language            = "language"
    

}
