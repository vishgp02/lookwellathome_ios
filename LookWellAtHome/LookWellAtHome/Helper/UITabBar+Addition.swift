//
//  UITabBar+Addition.swift
//  TheDon
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Ashish Chauhan. All rights reserved.
//

import UIKit

import UIKit


// MARK: - Methods
public extension UITabBar {
    
    /// Set tabBar colors.
    ///
    /// - Parameters:
    ///   - background: background color.
    ///   - selectedBackground: background color for selected tab.
    ///   - item: icon tint color for items.
    ///   - selectedItem: icon tint color for item.
    public func setColors(background: UIColor? = nil,
                          selectedBackground: UIColor? = nil,
                          item: UIColor? = nil,
                          selectedItem: UIColor? = nil) {
        
        // background
        self.barTintColor = background ?? self.barTintColor
        
        // selectedItem
        self.tintColor = selectedItem ?? self.tintColor
        //        self.shadowImage = UIImage()
        self.backgroundImage = UIImage()
        self.isTranslucent = false
        
        // selectedBackgoundColor
        
        if let _ = selectedBackground {
            let _ = CGSize(width: self.frame.width/CGFloat(self.items!.count), height: self.frame.height)
            //self.selectionIndicatorImage = UIImage(color: selectedbg, size: rect)
        }
        
        if let itemColor = item {
            for barItem in self.items! as [UITabBarItem] {
                // item
                if let _ = barItem.image {
                    //barItem.image = image.filled(withColor: itemColor).withRenderingMode(.alwaysOriginal)
                    barItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : itemColor], for: .normal)
                    
//                       barItem.setTitleTextAttributes([NSAttributedStringKey.font : AppFontMarCel.Regular.size(10)], for: .normal)
                    
                    if let selected = selectedItem {
                        barItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : selected], for: .selected)
                    }
                }
            }
        }
    }
    
    

}

class CustomTabBar: UITabBar {
    
    
    override func awakeFromNib() {
          super.awakeFromNib()
    }
    

}


