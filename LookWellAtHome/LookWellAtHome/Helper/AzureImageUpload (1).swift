//
//  AzureImageUpload.swift
//  CRIIIO
//
//  Created by Sachin on 22/10/18.
//  Copyright © 2018 TechAhead. All rights reserved.
//

import Foundation
import AZSClient
import Photos

enum StoreContainer: String {
    case ProfilePic = "profilepic"
    case GameActivity = "gameactivity"
}

class AzureImageUpload {

 
    
    static let connectionString = "DefaultEndpointsProtocol=https;AccountName=toothblob;AccountKey=QmKBJgfZKH8qzRbCAzx8bnTYAx9WqFaqfSVMcQZcVUkyP2eZEEB0Cm5mnJqKxdQhTxAa26BZwIP+XNW8vrYbQg=="
    static let containerName = "toothfarecontainer"
    static let azureBlobURL = "https://toothblob.blob.core.windows.net/" + containerName + "/";
    

    //=========

    // MARK: connection string for Azure Blob

    class func uploadProfileImage(image: UIImage, onCompletion: @escaping (_ mediaSrcName: String?, _ error: Error?) -> Void) {
            do {
                let account = try AZSCloudStorageAccount(fromConnectionString: connectionString)
                let blobClient = account.getBlobClient()
                let blobContainer = blobClient.containerReference(fromName: containerName)
                 let fileName = getRandomImageName(fileExtension: "JPG")
                
                if let compresedData:Data = image.jpegData(compressionQuality: 0.4) {
                              let blockBlob: AZSCloudBlockBlob = blobContainer.blockBlobReference(fromName: fileName)
                              blockBlob.upload(from: compresedData as Data, completionHandler: { (error) in
                                  if error == nil {
                                      onCompletion(fileName, nil)
                                  } else {
                                      onCompletion(nil, error)
                                  }
                              })
                          } else if let data = image.jpegData(compressionQuality: 1) {
                              let blockBlob: AZSCloudBlockBlob = blobContainer.blockBlobReference(fromName: fileName)
                              blockBlob.upload(from: data as Data, completionHandler: { (error) in
                                  if error == nil {
                                      onCompletion(fileName, nil)
                                  } else {
                                      onCompletion(nil, error)
                                  }
                              })
                          } else {
                              let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "unable to upload"])
                              onCompletion(nil, error)
                          }
            } catch {
                print(error)
            }

        }
    
    

    private class func getRandomImageName(fileExtension: String = "JPG") -> String {
        let timestamp = String(Int(NSDate().timeIntervalSince1970))
        return timestamp + ".\(fileExtension)"
    }
}
