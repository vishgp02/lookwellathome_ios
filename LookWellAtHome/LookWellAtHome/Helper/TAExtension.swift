//
//  TAExtension.swift
//  Givet
//
//  Created by Rahul Gupta on 12/06/17.
//  Copyright © 2017 RahulGupta. All rights reserved.
//

import UIKit

class TAExtension: NSObject {
    
}

import UIKit
import CoreData

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
    
}

extension UIView
{
//    func setViewShadow()
//    {
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOffset = CGSize(width: 0, height: 5.0)
//        self.layer.shadowOpacity = 0.3
//        self.layer.shadowRadius = 5
//        self.layer.masksToBounds = false
//    }
    
    func removeViewShadow()
    {
        self.layer.masksToBounds = true
    }
    
    func makeCircular()
    {
        layer.masksToBounds = true
        layer.cornerRadius = frame.size.width/2
    }
    
    func makeCircularView()
    {
        layer.cornerRadius = frame.size.width/2
    }
    
    func convertToImage() -> UIImage?
    {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0);
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return img;
    }
    
    
    
        func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
            layer.masksToBounds = false
            layer.shadowOffset = offset
            layer.shadowColor = color.cgColor
            layer.shadowRadius = radius*screenScaleFactor
            layer.shadowOpacity = opacity
            let backgroundCGColor = backgroundColor?.cgColor
            backgroundColor = nil
            layer.backgroundColor =  backgroundCGColor
        }
    
}


extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
   @IBInspectable
     var shadowRadius: CGFloat {
         get {
             return layer.shadowRadius
         }
         set {
             
             layer.shadowRadius = shadowRadius
         }
     }
   
     @IBInspectable
     var shadowColor : UIColor{
         get{
             return UIColor.init(cgColor: layer.shadowColor!)
         }
         set {
             layer.shadowColor = newValue.cgColor
         }
     }
    
    @IBInspectable
     var borderColor : UIColor{
         get{
             return UIColor.init(cgColor: layer.shadowColor!)
         }
         set {
             layer.shadowColor = newValue.cgColor
         }
     }
 
    
    /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
     * [0,1] range will give undefined results. Animatable. */
    @IBInspectable var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    /* The shadow offset. Defaults to (0, -3). Animatable. */
    @IBInspectable var shadowOffset: CGPoint {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
        }
    }
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
//    @IBInspectable var shadowRadius: CGFloat {
//        set {
//            layer.shadowRadius = newValue
//        }
//        get {
//            return layer.shadowRadius
//        }
//    }
}



extension UIImage
{
    var uncompressedPNGData: Data?      { return self.pngData()        }
    var highestQualityJPEGNSData: Data? { return self.jpegData(compressionQuality: 1.0)  }
    var highQualityJPEGNSData: Data?    { return self.jpegData(compressionQuality: 0.75) }
    var mediumQualityJPEGNSData: Data?  { return self.jpegData(compressionQuality: 0.5)  }
    var lowQualityJPEGNSData: Data?     { return self.jpegData(compressionQuality: 0.25) }
    var lowestQualityJPEGNSData:Data?   { return self.jpegData(compressionQuality: 0.0)  }
    
    func thumbnailOfSize(size: CGFloat) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: size, height: size))
        let rect = CGRect(x: 0, y: 0, width: size, height: size)
        UIGraphicsBeginImageContext(rect.size)
        draw(in: rect)
        let thumbnail = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext()
        return thumbnail!
    }
    
    func resizeToWidth(width:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func toBase64()->String?
    {
        let data = self.jpegData(compressionQuality: 0.8)
        let strBase64 = data?.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
        return strBase64
    }
}

extension UIButton
{
    func setContentModeAspectFill()
    {
        self.contentMode = .scaleAspectFill
        self.contentHorizontalAlignment = .fill
        self.contentVerticalAlignment = .fill
    }
}

extension String
{
    
    var parseJSONString: AnyObject?
          {
              let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)

              if let jsonData = data
              {
                  // Will return an object or nil if JSON decoding fails
                  do
                  {
                      let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                      if let jsonResult = message as? NSMutableArray
                      {
                          print(jsonResult)

                          return jsonResult //Will return the json array output
                      }
                      else
                      {
                          return nil
                      }
                  }
                  catch let error as NSError
                  {
                      print("An error occurred: \(error)")
                      return nil
                  }
              }
              else
              {
                  // Lossless conversion of the string was not possible
                  return nil
              }
          }
    
    func toBool() -> Bool {
        switch self {
        case "True", "true", "yes","YES","1","TRUE":
            return true
        case "False", "false", "no","NO","0","FALSE":
            return false
        default:
            return false
        }

    }
    func toBoolString() -> String {
        switch self {
        case "True", "true", "yes","YES","1","TRUE":
            return "1"
        case "False", "false", "no","NO","0","FALSE":
            return "0"
        default:
            return "0"
        }
    }
    
    
    static func className(_ aClass: AnyClass) -> String
    {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int, to: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        let end = index(start, offsetBy: to - from)
        return String(self[start ..< end])
    }
    
    func substring(range: NSRange) -> String {
        return substring(from: range.lowerBound, to: range.upperBound)
    }
    
    var length: Int {
        return self.count
    }
    
    static func localizedString(_ string: String) -> String
    {
        return NSLocalizedString(string, comment: "")
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
 
    func object() -> [String:AnyObject]
    {
        let data: Data? = self.data(using: String.Encoding.utf8)
        do {
            let resultJson = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
            return resultJson!
        } catch let error as NSError
        {
            print(error)
            return [String:AnyObject]()
        }
    }
    
    var lastPathComponent: String {
        get {
            return (self as NSString).lastPathComponent
        }
    }
    var pathExtension: String {
        get {
            return (self as NSString).pathExtension
        }
    }
    var stringByDeletingLastPathComponent: String {
        get {
            return (self as NSString).deletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        get {
            return (self as NSString).deletingPathExtension
        }
    }
    var pathComponents: [String] {
        get {
            return (self as NSString).pathComponents
        }
    }
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        let nsSt = self as NSString
        return nsSt.appendingPathExtension(ext)
    }
    
    func trimString(byString string:String) -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    
    func toJSONObject() -> Any?
    {
        let data = self.data(using: String.Encoding.utf8)
        return data?.formattedJSON()
    }
    
    func toImage() -> UIImage?
    {
        let dataDecoded : Data = Data(base64Encoded: self, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage
    }
    
    func hexadecimal() -> Data?
    {
        var data = Data(capacity: count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSMakeRange(0, utf16.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)!
            data.append(&num, count: 1)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
    
    private func matches(pattern: String) -> Bool {
        let regex = try! NSRegularExpression(
            pattern: pattern,
            options: [.caseInsensitive])
        return regex.firstMatch(
            in: self,
            options: [],
            range: NSRange(location: 0, length: utf16.count)) != nil
    }
    func isValidURL() -> Bool {
        guard let url = URL(string: self) else { return false }
        if !UIApplication.shared.canOpenURL(url) {
            return false
        }
        let urlPattern = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$"
        return self.matches(pattern: urlPattern)
    }
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    
    func isAlphanumeric(ignoreDiacritics: Bool = false) -> Bool {
        if ignoreDiacritics {
            return self.range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil && self != ""
        }
        else {
            return self.isAlphanumeric()
        }
    }
    
    var isMustAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    func isValidPassword()-> Bool{
    return !isEmpty && self.length > 8 && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    

}

extension Bool
{
    func boolToInt() -> Int
    {
        if self {
            return 1
        }
        return 0
    }
}

extension Array where Element: Equatable
{
    mutating func removeObject(object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object: object)
        }
    }
}

extension Array
{
    func toJSONString() -> String?
    {
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)!
        }
        catch
        {
            return nil
        }
    }
}

extension Dictionary
{
    func toJSONString() -> String?
    {
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)!
        }
        catch
        {
            return nil
        }
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension Data
{
    func formattedJSON() -> Any?
    {
        do
        {
            let dict = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.allowFragments)
            return dict
        }
        catch
        {
            return nil
        }
    }
    
    func hexadecimal() -> String
    {
        return map { String(format: "%02x", $0) }
            .joined(separator: "")
    }
}

extension Date
{
    func toUTC() -> Date
    {
        let tz = NSTimeZone.default
        let seconds = -tz.secondsFromGMT(for: self)
        return Date(timeInterval: TimeInterval(seconds), since: self)
    }
}

extension UITextField
{
    func addToolbarWithButtonTitled(title:String, forTarget: UIViewController, selector:Selector)
    {
        let flexibleSpaceLeft = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let barBtnDone = UIBarButtonItem.init(title: title, style: .done, target:forTarget, action: selector)
        
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: 40.0))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        toolBar.setItems([flexibleSpaceLeft,barBtnDone], animated: false)
        self.inputAccessoryView = toolBar
    }
}

extension UITableView
{
    func addWatermark(with text:String)
    {
        let label = UILabel(frame: CGRect(x: 5.0, y: 0.0, width: self.bounds.size.width - 5.0, height: self.bounds.size.height))
        label.text = text
        label.font = UIFont(name: "Helvetica Neue Bold", size: 20.0)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        self.backgroundView = label
    }
    
    func stopFloatingSectionHeader()
    {
        let dummyViewHeight: CGFloat = 40
        let dummyView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.bounds.size.width), height: dummyViewHeight))
        self.tableHeaderView = dummyView
        contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
    }
}

extension UICollectionView
{
    func addWatermark(with text:String)
    {
        let label = UILabel(frame: CGRect(x: 5.0, y: 0.0, width: self.bounds.size.width - 5.0, height: self.bounds.size.height))
        label.text = text
        label.font = UIFont(name: "Helvetica Neue Bold", size: 20.0)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        self.backgroundView = label
    }
}

extension Data {
    var format: String {
        let array = [UInt8](self)
        let ext: String
        switch (array[0]) {
        case 0xFF:
            ext = "jpg"
        case 0x89:
            ext = "png"
        case 0x47:
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "unknown"
        }
        return ext
    }
}
protocol JSONAble {}
extension JSONAble
{
    func toDictionary() -> [String:Any]
    {
        var dict = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children
        {
            if let key = child.label
            {
                dict[key] = child.value
            }
        }
        return dict
    }
}



extension Notification.Name {
    static let notificationSocketOnline = Notification.Name("notificationSocketOnline")
    static let notificationRoomConnect = Notification.Name("notificationRoomConnect")
    static let notificationSendMessage = Notification.Name("notificationSendMessage")
    static let notificationSocketTyping = Notification.Name("notificationSocketTyping")
    static let notificationSocketConnected = Notification.Name("notificationSocketConnected")
    static let notificationRecieverAcknowledge = Notification.Name("RecieverAcknowledge")
    static let notificationLocataionSettingUpdated = Notification.Name("locationSettingUpdated")
    static let notificationCheckInUserNotify = Notification.Name("checkInUserListUpdated")
}

extension UIStoryboard{
    class func loadViewController<T:UIViewController>(storyBoardName:String,identifierVC:String,type:T.Type,function : String = #function) -> T
    {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: identifierVC) as? T else
        {
            fatalError("ViewController with identifier \(identifierVC), not found in  Storyboard.\nFile : \(storyBoardName) \n\nFunction : \(function)")
        }
        return controller
    }
}

//MARK: UITableView relaod data extension
extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
    func isCellVisible(section:Int, row: Int) -> Bool {
        guard let indexes = self.indexPathsForVisibleRows else {
            return false
        }
        return indexes.contains {$0.section == section && $0.row == row }
    }
}

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
}

extension UIFont {
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: [NSAttributedString.Key.font: self],
                                                     context: nil).size
    }
}

enum StoryboardType: String {
    case Main
    case Profile
    case HomeTabBar
    case Session
    case Home
    case MyProfile
    case Setting
    var fileName : String{
        return rawValue
    }
}

extension UINavigationController {
    
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
}

extension UILabel {
    func scaleFont()  {
        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font!.pointSize) * screenScaleFactor)
    }
}


extension Array
{
    func uniqueArrayElement<S: Sequence, E: Hashable>(source: S) -> [E] where E==S.Iterator.Element {
        var seen: [E:Bool] = [:]
        return source.filter({ (v) -> Bool in
            return seen.updateValue(true, forKey: v) == nil
        })
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround(_ tableView:UITableView) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
//    func presentDetail(_ viewControllerToPresent: UIViewController) {
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        self.view.window!.layer.add(transition, forKey: kCATransition)
//        present(viewControllerToPresent, animated: false)
//    }
//    
//    func dismissDetail() {
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromRight
//        self.view.window!.layer.add(transition, forKey: kCATransition)
//        dismiss(animated: false)
//    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
