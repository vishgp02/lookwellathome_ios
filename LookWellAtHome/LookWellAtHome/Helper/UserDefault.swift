//
//  UserDefault.swift
//  HealthyMummy
//
//  Created by Shashank Gupta on 6/23/17.
//  Copyright © 2017 TechAhead. All rights reserved.
//

import Foundation

extension UserDefaults {
    class var deviceToken: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "deviceToken")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "deviceToken") as? String
        }
    }

    class var cityName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "cityName")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "cityName") as? String
        }
    }
    
    class var firstName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "firstName")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "firstName") as? String
        }
    }
    


    class var cityCode: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "cityCode")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "cityCode") as? String
        }
    }

    class var orderType: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "orderType")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "orderType") as? String
        }
    }

    class var loginEmail: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "loginEmail")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "loginEmail") as? String
        }
    }
    
    class var loginPhone: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "loginPhone")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "loginPhone") as? String
        }
    }
    
    class var accessToken: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "accessToken")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "accessToken") as? String
        }
    }
    
    class var isFirstTime: Bool? {
        set {
            UserDefaults.standard.set(newValue, forKey: "isFirstTime")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "isFirstTime") as? Bool
        }
    }

    class var isLoggedIn: Bool? {
        set {
            UserDefaults.standard.set(newValue, forKey: "isLoggedIn")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool
        }
    }

    class func removeAllUserDefaults() {
        let deviceToken = UserDefaults.deviceToken

        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.deviceToken = deviceToken

        UserDefaults.standard.synchronize()
    }
    
}
