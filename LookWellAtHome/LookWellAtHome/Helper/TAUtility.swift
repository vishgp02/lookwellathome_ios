//
//  TAUtility.swift
//  ToothFare
//
//  Created by Pankaj Kumar on 09/09/19.
//  Copyright © 2019 Pankaj Kumar. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import Toaster
import SVProgressHUD
import UserNotifications
import AVFoundation
import Photos
import AVKit
import ReachabilitySwift
import Toast_Swift

class TAUtility: NSObject {
    /**
     *  Method to Check Internet Reachability
     */
    
    
    internal static let sharedInstance: TAUtility = {
        
        return TAUtility()
    }()
    
    class func performTaskInMainQueue(task:@escaping () -> Void) {
        DispatchQueue.main.async {
            task()
        }
    }

    class func isInternetReachable() -> Bool {
        let reachability: Reachability? = Reachability()
        
        var isInternetAvailable: Bool
        
        if let _ = reachability {
            if reachability!.isReachable {
                if reachability!.isReachableViaWiFi {
                    isInternetAvailable = true
                    //print("TAUtility :: Reachable via WiFi")
                } else if reachability!.isReachableViaWWAN {
                    isInternetAvailable = true
                    //print("TAUtility :: Reachable via WAN")
                } else {
                    isInternetAvailable = true
                    //print("TAUtility :: Reachable via Cellular")
                }
            } else {
                isInternetAvailable = false
                //print("TAUtility :: Not reachable")
            }
        } else {
            isInternetAvailable = false
        }
        return isInternetAvailable
        //return self.isConnectedToNetwork()
    }

    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        return ret
    }

    /**
     *  Method to Get the Root View Controller
     *  @return: Root View Controller Object
     */
    
    class func rootViewController() -> UIViewController {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    
    /**
     *  Method to Get Top Most View Controller
     *  @param: rootViewController
     *  @return: top most UIViewController object
     */
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController {
        if let navigationController = rootViewController as? UINavigationController {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        if let tabBarController = rootViewController as? UITabBarController {
            if let selectedTabBarController = tabBarController.selectedViewController {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        if let presentedViewController = rootViewController.presentedViewController {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }

    class func showToast(message: String?) {
        DispatchQueue.main.async {
            if message != nil {
                if (message != "") && (message != kMsgServiceNotResponding) {
                    ToastCenter.default.cancelAll()
                    ToastView.appearance().font = .systemFont(ofSize: 15)
                    ToastView.appearance().backgroundColor = .red
                    Toast(text: message, duration: Delay.short).show()
                }
            }
        }
    }
    
    class func showToastMessage(message: String?, view: UIView) {
           DispatchQueue.main.async {
               if message != nil {
                   if (message != "") && (message != kMsgServiceNotResponding) {
                     
                    var style = ToastStyle()
                    
                    
                    view.hideAllToasts()
                    // this is just one of many style options
                    style.messageColor = .white

                    // present the toast with the new style
                    view.makeToast(message, duration: 3.0, position: .bottom, style: style)

                    // or perhaps you want to use this style for all toasts going forward?
                  

                    // toggle "tap to dismiss" functionality
                    ToastManager.shared.isTapToDismissEnabled = true

                    // toggle queueing behavior
                    ToastManager.shared.isQueueEnabled = true

                    
                   }
               }
           }
       }

    class func showToastMessageFromTop(message: String?, view: UIView) {
           DispatchQueue.main.async {
               if message != nil {
                   if (message != "") && (message != kMsgServiceNotResponding) {
                     
                    var style = ToastStyle()
                    
                    
                    view.hideAllToasts()
                    // this is just one of many style options
                    style.messageColor = .white

                    // present the toast with the new style
                    view.makeToast(message, duration: 3.0, position: .top, style: style)

                    // or perhaps you want to use this style for all toasts going forward?
                  

                    // toggle "tap to dismiss" functionality
                    ToastManager.shared.isTapToDismissEnabled = true

                    // toggle queueing behavior
                    ToastManager.shared.isQueueEnabled = true

                    
                   }
               }
           }
       }
    
    class func showToastLongerDuration(message: String?) {
        DispatchQueue.main.async {
            if message != nil {
                if (message != "") && (message != kMsgServiceNotResponding) {
                    
                    ToastCenter.default.cancelAll()
                    ToastView.appearance().font = .systemFont(ofSize: 15)
                    ToastView.appearance().backgroundColor = .black
                    Toast(text: message, duration: Delay.long).show()
                }
            }
        }
    }
    
    /**
     *  Method to Show Alert with ok Button
     *  @param: title of Alert
     *  @param: message of Alert
     */
    
    
    class func showAlertFor(title: String,cancelButtonTitle: String, otherButtonTitle: String, message: String, obj: UIViewController?, completion: @escaping (_ tag: Int) -> Void) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: cancelButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(0)
        }))
        alert.addAction(UIAlertAction.init(title: otherButtonTitle, style: UIAlertAction.Style.default, handler: { (action : UIAlertAction) in
            completion(1)
        }))
        
        if let _ = obj
        {
            obj!.present(alert, animated: true, completion: {})
        }
        else
        {
//            if topViewController != nil {
//                       let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//                       let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: handler)
//                       alertController.addAction(cancelAction)
//                       let topViewController: UIViewController = self.topMostViewController(rootViewController: self.rootViewController())
//                       topViewController.present(alertController, animated: true, completion: nil)
//                   } else {
//                       Debug.log.error("TAUtility :: showActivity :: Unable to get Top Most View Controller")
//                   }
            
            let topViewController: UIViewController = self.topMostViewController(rootViewController: self.rootViewController())
            topViewController.present(alert, animated: true, completion: nil)
            
//            kAppDelegate?.window?.rootViewController?.present(alert, animated: true, completion: nil)

        }
    }
    
    class func showOkAlert(title: String?, message: String?, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let topViewController: UIViewController? = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
        
        if topViewController != nil {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: handler)
            alertController.addAction(cancelAction)
            let topViewController: UIViewController = self.topMostViewController(rootViewController: self.rootViewController())
            topViewController.present(alertController, animated: true, completion: nil)
        } else {
            Debug.log.error("TAUtility :: showActivity :: Unable to get Top Most View Controller")
        }
    }
    class func showOkAlertNew(title: String?, message: String?, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: handler)
        alertController.addAction(cancelAction)
        kAppDelegate?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }

//    class func showActivity(message: String) {
//        let topViewController: UIViewController? = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
//
//        if let _ = topViewController {
//            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
//
//            alert.view.tintColor = UIColor.black
//            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
//            loadingIndicator.hidesWhenStopped = true
//            if #available(iOS 13.0, *) {
//                loadingIndicator.style = UIActivityIndicatorView.Style.medium
//            } else {
//                loadingIndicator.style = .medium
//                // Fallback on earlier versions
//            }
//            loadingIndicator.startAnimating()
//            alert.view.addSubview(loadingIndicator)
//            topViewController!.present(alert, animated: false, completion: nil)
//        } else {
//            Debug.log.error("TAUtility :: showActivity :: Unable to get Top Most View Controller")
//        }
//    }
    
    class func hideActivity() {
        let topViewController: UIViewController? = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
        if let _ = topViewController {
            topViewController!.dismiss(animated: false, completion: nil)
        } else {
            Debug.log.error("TAUtility :: hideActivity :: Unable to get Top Most View Controller")
        }
    }
    
    class func isValidEmail(testStr: String) -> Bool {
        //let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        //let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        //let emailRegEx = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$"
        let emailRegEx = "\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,64}\\z"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr.lowercased())
    }
    
    class func isValidPassword(password: String?) -> Bool {
        if password == nil {return false}
        if password!.count < 12 {return false}
        
        if !self.isContainAtleastOneSpecialCharacter(string: password!) {return false}
        if self.isContaintAtleastOneSmallLetter(string: password) || self.isContaintAtleastOneCapitalLetter(string: password) {
            
        } else {
            return false
        }
//        if !self.isContaintAtleastOneCapitalLetter(string: password!) {return false}
//        if !self.isContaintAtleastOneSmallLetter(string: password!) {return false}
        if !self.isContaintAtleastOneNumber(string: password!) {return false}
        
        return true
    }
    
    class func isValidMobileNumber(number: String?) -> Bool {
        if number == nil {return false}
        if number!.count != 10 {return false}
        
        return true
    }
    
    class func test(_ password: String, matches: String) -> Bool {
        return password.range(of: matches, options: .regularExpression) != nil
    }
    
    class func checkPasswordStrength(password: String) ->  Bool{
        
        let score = strengthOfPassword(password: password)
        switch(score) {
        case 0: return false // too weak
        case 1...2: return false //weak
        case 3...4: return  true //strong
        default: return true //too good
        }
    }

    class func strengthOfPassword(password: String) -> Int {
        var score = 0
        
        // At least one lowercase letter
        if test(password, matches: "[a-züöäß]") {
            score = 1
        }
        
        // At least one uppercase
        if test(password, matches: "[A-ZÜÖÄß]") {
            score = 1
        }
        
        // At least one number
        if test(password, matches: "[0-9]") {
            score = 1
        }
        
        // At least one special character
        if test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]") {
            score = 1
        }
        if test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"), test(password, matches: "[0-9]") || test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"),test(password, matches: "[A-ZÜÖÄß]") || test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"),  test(password, matches: "[a-züöäß]"), test(password, matches: "[0-9]"),test(password, matches: "[A-ZÜÖÄß]") || test(password, matches: "[0-9]"),test(password, matches: "[a-züöäß]") || test(password, matches: "[A-ZÜÖÄß]"),test(password, matches: "[a-züöäß]"){
            score = 2
        }
        if password.count >= 8 ,test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]") {
            score = 2
        }
        if password.count >= 8 ,test(password, matches: "[0-9]") {
            score = 2
        }
        if password.count >= 8 ,test(password, matches: "[A-ZÜÖÄß]") {
            score = 2
        }
        if password.count >= 8 ,test(password, matches: "[a-züöäß]") {
            score = 2
        }
        
        //if test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"), test(password, matches: "[0-9]"), test(password, matches: "[A-ZÜÖÄß]"), test(password, matches: "[a-züöäß]"){
        //  score = 3
        //}
        if password.count >= 8  ,test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]") || test(password, matches: "[0-9]"),test(password, matches: "[A-ZÜÖÄß]"), test(password, matches: "[a-züöäß]"){
            score = 3
        }
        if password.count >= 8 ,test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"), test(password, matches: "[0-9]"),test(password, matches: "[A-ZÜÖÄß]"), test(password, matches: "[a-züöäß]"){
            score = 4
        }
        if password.count >= 8  ,test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]") ||  test(password, matches: "[0-9]"),test(password, matches: "[A-ZÜÖÄß]") || test(password, matches: "[a-züöäß]"){
            score = 3
        }
        
        // A length of at least 8 characters
        if password.count >= 12 ,test(password, matches: "[^A-Za-z0-9üöäÜÖÄß]"), test(password, matches: "[0-9]"),test(password, matches: "[A-ZÜÖÄß]"), test(password, matches: "[a-züöäß]"){
            score = 5
            
        } else if password.count < 8 {
            score = 0
        }
        return score
    }
    
    class func isContainAtleastOneSpecialCharacter(string: String?) -> Bool {
        if string == nil {return false}
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format: "SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: string!)
        return specialresult
    }

    class func isContaintAtleastOneCapitalLetter(string: String?) -> Bool {
        if string == nil {return false}
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: string!)
        return capitalresult
    }

    class func isContaintAtleastOneSmallLetter(string: String?) -> Bool {
        if string == nil {return false}
        
        let capitalLetterRegEx  = ".*[a-z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: string!)
        return capitalresult
    }

    class func isContaintAtleastOneNumber(string: String?) -> Bool {
        if string == nil {return false}
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: string!)
        return numberresult
    }

    class func validString(string: Any?) -> String {
        let str: String? = string as? String
        
        if str == nil {
            return ""
        } else if str == "<null>" || str == "<NULL>" {
            return ""
        } else if str == "<nil>" || str == "<NIL>" {
            return ""
        } else if str == "null" || str == "NULL" {
            return ""
        } else if str == "NIL" || str == "nil" {
            return ""
        } else if str == "(null)" {
            return ""
        }
        return str!
    }

    
    class func showLoader(message: String) {
        
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
            kAppDelegate?.window?.isUserInteractionEnabled = false
            if message != kPleaseWait {
                SVProgressHUD.show(withStatus: message)
            } else {
                SVProgressHUD.show()
            }
        }
    }
    
    class func hideLoader() {

        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.none)
            SVProgressHUD.dismiss()
            kAppDelegate?.window?.isUserInteractionEnabled = true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            kAppDelegate?.window?.isUserInteractionEnabled = true
        }
    }
    
    class func paddingTextField(arrTxtFields: [UITextField]) {
        for txtField in arrTxtFields {
            let paddingForFirst = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtField.frame.size.height))
            txtField.leftView = paddingForFirst
            txtField.leftViewMode = UITextField.ViewMode.always
            txtField.layer.cornerRadius = 5.0
            txtField.layer.borderWidth = 1.0
            txtField.layer.borderColor = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0).cgColor
        }
    }
    
    class func isSimulatorRunning() -> Bool {
        #if targetEnvironment(simulator)
            return true
        #else
            return false
        #endif
    }
    
    class func trimWhiteSpaceAndNewLine(str: String?) -> String {
        if str == nil {return ""}
        return str!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    class func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }

    class func arrayToJsonString(array: [Any]?) -> String? {
        if array == nil {return nil}
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: array!, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                DLog(message: "Json String :: \(JSONString)")
                return JSONString
            }
        } catch {
            return nil
        }
        return nil
    }
    
    class func convertJsonStringToArray(text: String?) -> [[String: Any]]? {
        if text == nil {return nil}
        if let data = text!.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                Debug.log.error(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func isLocationServiceEnable() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied {
                return false
            } else if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                return true
            }
        } else {
            return false
        }
        return false
    }
    
    class func isCameraServiceEnable() -> Bool {
        var isServiceEnable = false
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch authStatus {
            case .authorized:
                isServiceEnable = true
            case .denied:
                isServiceEnable = false
            case .notDetermined:
                isServiceEnable = false
            default:
                isServiceEnable = false
            }
        } else {
            isServiceEnable = false
        }
        return isServiceEnable
    }
    
    class func isPhotoServiceEnable() -> Bool {
        var isServiceEnable = false
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
        case .authorized:
            isServiceEnable = true
        case .denied:
            isServiceEnable = false
        case .notDetermined:
            isServiceEnable = false
        default:
            isServiceEnable = false
        }
        return isServiceEnable
    }
    
    class func getQueryStringParameter(url: String?, param: String) -> String? {
        if let url = url, let urlComponents = URLComponents(string: url), let queryItems = (urlComponents.queryItems) {
            return queryItems.filter({ (item) in item.name == param }).first?.value!
        }
        return nil
    }
    // MARK: - Conversion
    class func toString(object: Any?) -> String {
        if let str = object as? String {
            return String(format: "%@", str)
        }
        
        if let num = object as? NSNumber {
            return String(format: "%@", num)
        }
        
        return ""
    }
    
    class func toBool(_ object: Any?) -> Bool {
        if let obj = object as? NSObject {
            let string = NSString(format: "%@", obj)
            return string.boolValue
        }
        return false
    }
    
    class func toInt(_ object: Any?) -> Int {
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Int(string) ?? 0
        }
        return 0
    }
    
    class func toInt64(_ object: Any?) -> Int64 {
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Int64(string) ?? 0
        }
        return 0
    }
    
    class func toDouble(_ object: Any?) -> Double {
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Double(string) ?? 0.0
        }
        return 0.0
    }
    
    class func toFloat(_ object: Any?) -> Float {
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            let floatValue = Float(string)
            let twoDecimalPlaces = String(format: "%.4f", floatValue ?? 0)
            return Float(twoDecimalPlaces) ?? 0.0
        }
        return 0.0
    }
    //  Converted with Swiftify v1.0.6414 - https://objectivec2swift.com/
    class func getEmojiEncodedText(_ text: String) -> String {
            let data: Data? = text.data(using: String.Encoding.nonLossyASCII)
            let goodValue = String(data: data!, encoding: String.Encoding.utf8)
            return goodValue!
    }

    class func getDecodedEmojiText(_ text: String) -> String {
        
        let data1: Data? = text.data(using: String.Encoding.utf8)
        let goodValue = String(data: data1!, encoding: String.Encoding.nonLossyASCII)
        if goodValue == nil {
            return text
        }
        return goodValue!
    }
    
    
    class func getDateString(date: Date, format: String) -> String {
        let dateF = DateFormatter()
        dateF.dateFormat = format
        //dateF.timeZone = TimeZone(abbreviation: "UTC")
        let dateStr = dateF.string(from: date)
        return dateStr
    }
    
//    class func getDateFromString(strDate: String, format: String) -> Date {
//        let dateF = DateFormatter()
//        dateF.dateFormat = format
//        //dateF.timeZone = TimeZone(abbreviation: "UTC")
//        
//        if let date = dateF.date(from: strDate) {
//            return date
//        } else {
//            return TAUtility.currentDate()
//        }
//    }
    
    class func getNearestValue(_ value: Double) -> Double {
        let numberOfPlaces = String(Int(value)).count - 1
        let multiplier = Double(pow(10.0, numberOfPlaces).description)
        return ceil(value / multiplier!) * multiplier!
    }
    
    class func stringToJSON(jsonText: String, encoding: String.Encoding = String.Encoding.utf8) -> Any? {
        if let data = jsonText.data(using: encoding) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                Debug.log.error(error)
            }
        }
        return nil
    }
    
    class func getVersionNumber() -> String {
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        if let version = nsObject as? String {
            return version
        } else {
            return ""
        }
    }
    
    class func getBundleIdentifier() -> String {
        if let identifier = Bundle.main.bundleIdentifier {
            //print("Identifier: ", identifier)
            return identifier
        } else {
            return ""
        }
    }
    
    class func isIphoneX() -> Bool {
        if (ISIPHONEX || ISIPHONEXR || ISIPHONEXSMax) {
            return true
        }
        return false
    }
    
  
//    class func currentDate()-> Date {
//        let dateF = DateFormatter()
//        dateF.dateFormat = kDateStyle
//        //dateF.timeZone = TimeZone(abbreviation: "UTC")
//        if let strDate: String = TAUtility.getDateString(date: Date(), format: kDateStyle) {
//            if let date: Date = TAUtility.getDateFromString(strDate: strDate, format: kDateStyle) {
//                return date
//            }
//        }
//        return Date()
//    }
    
    class public func toJsonString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    class func validate(forFloat string: String?, allowDigitsBeforeDot before: Int, allowDigitsAfterDot after: Int) -> Bool {
        let charSet = "0123456789."
        var s = CharacterSet(charactersIn: charSet)
        s = s.inverted
        let r: NSRange? = (string as NSString?)?.rangeOfCharacter(from: s)
        if r?.location == NSNotFound {
            let arrPart = string?.components(separatedBy: ".")
            if (arrPart?.count ?? 0) > 2 {
                return false
            }
            let strBeforeDot = arrPart?[0]
            var strAfterDot: String? = nil
            if (arrPart?.count ?? 0) > 1 {
                strAfterDot = arrPart?[1]
            }
            
            if (strBeforeDot?.count ?? 0) > before {
                return false
            }
            
            if (strAfterDot?.count ?? 0) > after {
                return false
            }
            return true
        }
        return false
    }

    
   class func convertDateFormaterForDate(date: String) -> String {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
           dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

           guard let date = dateFormatter.date(from: date) else {
               assert(false, "no date from string")
               return ""
           }

           dateFormatter.dateFormat = "dd/MM/yyyy"
           dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
           let timeStamp = dateFormatter.string(from: date)

           return timeStamp
       }
       
   class func convertDateFormaterForTime(time: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

            guard let date = dateFormatter.date(from: time) else {
                assert(false, "no date from string")
                return ""
            }

            dateFormatter.dateFormat = "h:mm a"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let timeStamp = dateFormatter.string(from: date)

            return timeStamp
        }
    
    class func convertDateFormater(time: String, format: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

            guard let date = dateFormatter.date(from: time) else {
                assert(false, "no date from string")
                return ""
            }

            dateFormatter.dateFormat = format
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let timeStamp = dateFormatter.string(from: date)

            return timeStamp
        }
    
    
    
}
