//
//  TwitterHelper.swift
//  SBMobile
//
//  Created by Archna on 20/12/18.
//  Copyright © 2018 Prodege. All rights reserved.
//

import UIKit
import TwitterKit

protocol TwitterHelperDelegate : class {
    func sharingCancelled() -> Void
    func sharingCompleted() -> Void
    func sharingFailed() -> Void
    func loginFlowFailed(error:Error) -> Void
}

class TwitterHelper: NSObject {
    
    var delegate : TwitterHelperDelegate?
    // MARK: Shared Instance
    static let shared = TwitterHelper()
     func showComposer(fromViewC:UIViewController?) -> Void {
        let composer = TWTRComposer()
        let strText = "ToothFARE"
        composer.setText(strText)
//        if let mediaUrl = vModel?.mediaURL {
//            composer.setURL(URL(string:mediaUrl))
//        }
//        if let downloadThumbnailImage = vModel?.videoThumbnailImg {
//             composer.setImage(downloadThumbnailImage)
//        }
        composer.show(from: fromViewC!, completion: { (result) in
            if result == TWTRComposerResult.cancelled {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    if self.delegate != nil {
                       self.delegate?.sharingCancelled()
                    }
                }
            } else if result == TWTRComposerResult.done {
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
//                    if self.delegate != nil {
//                        self.delegate?.sharingCompleted()
//                    }
//                }
                let client = TWTRAPIClient.withCurrentUser()
                     client.requestEmail { email, error in
                       if (email != nil) {
                           print("email: \(email)");
                       } else {
                         print("error: \(error!.localizedDescription)");
                       }
                     }
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    if self.delegate != nil {
                        self.delegate?.sharingFailed()
                    }
                }
            }
        })
    }
    
    func getEmail() {
        // Swift
        TWTRTwitter.sharedInstance().logIn { (session, error) in
                 if (session != nil) {
                     
                     let client = TWTRAPIClient.withCurrentUser()
                    client.requestEmail { email, error in
                         if (email != nil) {
                             print("signed in as \(String(describing: session?.userName))");
                             let firstName = session?.userName ?? ""   // received first name
                             let lastName = session?.userName ?? ""  // received last name
                             let recivedEmailID = email ?? ""   // received email
                           
                             
                         }else {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                                if self.delegate != nil {
                                    self.delegate?.loginFlowFailed(error: error!)
                                }
                            }
                             print("error: \(String(describing: error?.localizedDescription))");
                         }
                     }
                 }else {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        if self.delegate != nil {
                            self.delegate?.loginFlowFailed(error: error!)
                        }
                    }
                     print("error: \(String(describing: error?.localizedDescription))");
                 }
             }
         }
    

    func tweetOnTwitter(fromViewController:UIViewController) -> Void {
        DispatchQueue.main.async {
            self.getEmail()
            return
            
            if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                self.getEmail()
//                self.showComposer(fromViewC: fromViewController )
            } else {
                // Log in, and then check again
                TWTRTwitter.sharedInstance().logIn { session, error in
                    if session != nil { // Log in succeeded
                        self.getEmail()
                       // self.showComposer(fromViewC: fromViewController)
                    } else {
                        //error
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                            if self.delegate != nil {
                                self.delegate?.loginFlowFailed(error: error!)
                            }
                        }
                    }
                }
            }
        }
    }
//
    class func logoutAllTwitterSession() {
        DispatchQueue.main.async {
            for _:TWTRAuthSession in (TWTRTwitter.sharedInstance().sessionStore.existingUserSessions() as? [TWTRAuthSession])! {
            }
            if TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers() {
                let store : TWTRSessionStore = TWTRTwitter.sharedInstance().sessionStore
                let strUserId = store.session()?.userID
                TWTRTwitter.sharedInstance().sessionStore.logOutUserID(strUserId!)
                
            }
        }
    }
}
