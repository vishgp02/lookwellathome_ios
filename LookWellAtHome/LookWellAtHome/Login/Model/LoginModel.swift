//
//  LoginModel.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 TechAhead Software. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginModel: NSObject , Mappable {

   var authToken : String?
   var createdOn : String?
   var deviceToken : String?
   var dob : String?
   var email : String?
   var facebookSocialId : AnyObject?
   var familyDentalOffice : String?
   var financialInst : String?
   var fname : String?
   var gender : String?
   var googleSocialId : AnyObject?
   var id : Int?
   var instagramSocialId : AnyObject?
   var isDeleted : String?
   var isVerified : String?
   var jwtrefreshtoken : String?
   var jwttoken : String?
   var lname : String?
   var postalCode : String?
   var profilePic : String?
   var timezone : String?
   var twitterSocialId : AnyObject?
   var updatedOn : String?
   var username : String?
   var verifiedOn : AnyObject?
    var is_verified = false

    
    
    
    required init?(map: Map)
    {
        
    }
    
    func mapping(map: Map)
    {
        authToken <- map["auth_token"]
        createdOn <- map["created_on"]
        deviceToken <- map["device_token"]
        dob <- map["dob"]
        email <- map["email"]
        facebookSocialId <- map["facebook_social_id"]
        familyDentalOffice <- map["family_dental_office"]
        financialInst <- map["financial_inst"]
        fname <- map["fname"]
        gender <- map["gender"]
        googleSocialId <- map["google_social_id"]
        id <- map["id"]
        instagramSocialId <- map["instagram_social_id"]
        isDeleted <- map["is_deleted"]
        isVerified <- map["is_verified"]
        jwtrefreshtoken <- map["jwtrefreshtoken"]
        jwttoken <- map["jwttoken"]
        lname <- map["lname"]
        postalCode <- map["postal_code"]
        profilePic <- map["profile_pic"]
        timezone <- map["timezone"]
        twitterSocialId <- map["twitter_social_id"]
        updatedOn <- map["updated_on"]
        username <- map["username"]
        verifiedOn <- map["verified_on"]
        is_verified <- map["is_verified"]

        
    }
    
    class func formattedData(data: [String: AnyObject]) -> LoginModel? {
        return Mapper<LoginModel>().map(JSON:data)
    }
    
}
