//
//  LoginVM.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 TechAhead Software. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

protocol LoginModeling
{
    func getLoginData(_ user:UserLoginModel) -> [String:AnyObject]
    func LoginApi(_ LoginParam: [String: AnyObject],completionHandler: @escaping (_ responseData:LoginModel?) -> Void)
    func ForgotPassword(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void)
    func validateOTP(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void)
    func resendOTP(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void)
    func updatePassword(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void)

//    func authenticateApi(_ authParam: [String: AnyObject],completionHandler: @escaping (_ responseData:Bool) -> Void)
 }


class LoginVM: LoginModeling , BaseModeling {
     
    func getLoginData(_ user: UserLoginModel) -> [String : AnyObject]
    {
        var paramDict = [String:AnyObject]()
        paramDict =
            [
                "username": user.username as AnyObject,
                "password": user.password as AnyObject,
                "device_token": user.deviceToken as AnyObject,
                "device_type": kDeviceType as AnyObject,
                "login_type": kLoginType as AnyObject
        ]
        return paramDict
    }
    
    
    
    func LoginApi(_ LoginParam: [String : AnyObject], completionHandler: @escaping (LoginModel?) -> Void) {
        self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .Login(param: LoginParam)), completionHandler: { (response, success, message) in
            if success
            {
//                guard let result = response as? [String: AnyObject] else {return}
                guard let result = response as? [Any] else {return}

                let loginModel = LoginModel.formattedData(data:(result.first as? [String:AnyObject])!)
                completionHandler(loginModel)

            }else {
                completionHandler(nil)
            }
        })
    }
    
    func ForgotPassword(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void) {
        self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .forgotPassword(param: params)), completionHandler: { (response, success, message) in
             if success
             {
                 guard let result = response as? [String: AnyObject] else {return}
                 completionHandler(true)

             }else {
                 completionHandler(false)
             }
         })
     }
     
    
    func validateOTP(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void) {
           self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .verifyAccountOTP(param: params)), completionHandler: { (response, success, message) in
                if success
                {
                    guard let result = response as? [String: AnyObject] else {return}
                    completionHandler(true)

                }else {
                    completionHandler(false)
                }
            })
        }
    
    func resendOTP(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void) {
           self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .resendOTP(param: params)), completionHandler: { (response, success, message) in
                if success
                {
                    guard let result = response as? [String: AnyObject] else {return}
                    completionHandler(true)

                }else {
                    completionHandler(false)
                }
            })
        }
    
    
    func updatePassword(_ params: [String : AnyObject], completionHandler: @escaping (Bool?) -> Void) {
              self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .updatePassword(param: params)), completionHandler: { (response, success, message) in
                   if success
                   {
                       guard let result = response as? [String: AnyObject] else {return}
                       completionHandler(true)

                   }else {
                       completionHandler(false)
                   }
               })
           }
    
    
    
//    func authenticateApi(_ authParam: [String: AnyObject],completionHandler: @escaping (_ responseData:Bool) -> Void){
//
////        self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .authentication(param: authParam)), completionHandler: { (response, success, message) in
////            if success {
////                guard let result = response as? [[String: AnyObject]], let dict = result.first else {return}
////                if let strToken = dict["token"] as? String {
////                    Defaults[\.jwtToken] = strToken
////                    completionHandler(true)
////                }
////                completionHandler(false)
////            }
////        })
//    }
}
