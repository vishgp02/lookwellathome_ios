//
//  LoginVCDataSource.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Vishal Gupta. All rights reserved.
//

import Foundation

enum LoginVCDataSource
{
    case email
    case password
    case forgotpassord

    static let allLoginProfileField = [email,password,forgotpassord]
    static var loginModel : UserLoginModel?
    var user: UserLoginModel?
    {
        get { return LoginVCDataSource.loginModel }
        set { LoginVCDataSource.loginModel = newValue }
    }
    
    enum CellType {
        case textField
        case forgtPswrd
        case button
        case socialBtn
    }
    
    var title : String
    {
        switch self {
        case .email: return "Email"
        case .password : return "Password"
        default:
            return ""
        }
    }
    
    var value : String
    {
        switch self {
        case .email: return user?.username ?? ""
        case .password: return user?.password ?? ""
        default:
            return ""
        }
    }
    
    var cellSecureText : Bool
    {
        switch self {
        case .email: return false
        case .password : return true
        default:
            return false
        }
    }
     
    var cellType: CellType {
        switch self {
        case .email: return .textField
        case .password : return .textField
        case .forgotpassord : return .button
        }
    }
    
}

class UserLoginModel {
    var username  : String? = ""
    var password : String? = ""
    var loginType = ""
    var deviceToken = ""
    var deviceType = ""
    var socialId = ""
    var facebookId: String?
    var googleId: String?
    var twitterId: String?
    var email = ""
    var firstName = ""
    var lastName = ""

 }
