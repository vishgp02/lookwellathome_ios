//
//  ForgotPasswordTVC.swift
//  ToothFARE
//
//  Created by Vishal Gupta on 18/11/19.
//  Copyright © 2019 Techahead. All rights reserved.
//

import UIKit

protocol ForgotPasswordTVCDelegate: class {
    func tapForgotPassword()
}

class ForgotPasswordTVC: UITableViewCell {

    weak var delegate:ForgotPasswordTVCDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func tapForgotPassword(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.tapForgotPassword()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
