//
//  FormTextFieldTVC.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 03/0401/12/19.
//  Copyright © 2018 Vishal Gupta. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


protocol loginTextFieldDelegate: class {
    func valueChanged(_ data:LoginVCDataSource, _ text:String)
    func signUpValueChanged(_ data:SignUpVcDataSource, _ text:String)
//    func valueChangedForAddChildren(_ data:AddChildrenDataSource, _ text:String)
//    func dobSelectedInSignUp(_ data:SignUpVcDataSource)
//    func dobSelectedInAddChildren(_ data:AddChildrenDataSource)
//    func selectTimeZoneInSignUp(_ data:SignUpVcDataSource)
//    func valueChangeContactInfo(_ data:ParentsContactInfoDataSource, _ text:String)
//    func valueChangeForChangePassword(_ data:ChangePasswordDataSource, _ text:String)
}

extension loginTextFieldDelegate
{
    func valueChanged(_ data:LoginVCDataSource, _ text:String)
    {
        print("Sign In Delegate Missing")
    }
    func signUpValueChanged(_ data:SignUpVcDataSource, _ text:String) {
        print("Sign Up Delegate Missing")
    }
//    func valueChangedForAddChildren(_ data:AddChildrenDataSource, _ text:String) {
//        print("AddChildrenDataSource Delegate Missing")
//    }
//    func dobSelectedInSignUp(_ data:SignUpVcDataSource) {
//           print("dob Delegate Missing")
//       }
//    func dobSelectedInAddChildren(_ data:AddChildrenDataSource) {
//              print("dob Delegate Missing")
//          }
//    func selectTimeZoneInSignUp(_ data:SignUpVcDataSource) {
//             print("dselectTimeInSignUpob Delegate Missing")
//         }
//    func valueChangeForChangePassword(_ data:ChangePasswordDataSource, _ text:String) {
//          print("ChangePasswordDataSource Delegate Missing")
//      }
//    func valueChangeContactInfo(_ data:ParentsContactInfoDataSource, _ text:String)
//    {
//        print("ParentsContactInfoDataSource Delegate Missing")
//
//    }
}

class FormTextFieldTVC: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet var textField : SkyFloatingLabelTextField!
    @IBOutlet weak var errorLbl : CustomLabel!
    
    var formDataSource : LoginVCDataSource?
    var signUpDataSource: SignUpVcDataSource?
//    var addChildrenDatasource: AddChildrenDataSource?
//    var changePasswordDataSource: ChangePasswordDataSource?
//    var contactInfoDataSource: ParentsContactInfoDataSource?

    weak var delegate : loginTextFieldDelegate?
    var index : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //Login datasource
    func configureCell(_ data:LoginVCDataSource)
    {
        textField.delegate = self
        self.errorLbl.text = ""
        formDataSource = data
        textField.placeholder = data.title
        textField.isSecureTextEntry = data.cellSecureText
        textField.text = data.value
    }
    
    
//    //signup datasource
    func configureSignUpView(_ data:SignUpVcDataSource)
    {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 70, height: 60))
        let btnShow =  UIButton.init(frame: CGRect.init(x: 1, y: 0, width: 70, height: 60))
        btnShow.setImage(UIImage.init(named:"downArrow"), for: .normal)
        view.backgroundColor = UIColor.clear
        btnShow.isUserInteractionEnabled = false
        view.addSubview(btnShow)
        view.bringSubviewToFront(btnShow)

        self.errorLbl.text = ""
        signUpDataSource = data
        textField.text = data.value
        textField.placeholder = data.title
        textField.isSecureTextEntry = false
        textField.keyboardType = .default
        textField.returnKeyType = data.cellSecureText ? .done : .done
        textField.isUserInteractionEnabled = true

        if let data = signUpDataSource
        {
            switch data {
            case .password:
            textField.isSecureTextEntry = data.cellSecureText
            case .confirmPassword:
            textField.isSecureTextEntry = data.cellSecureText
            break
            case .phone:
                textField.keyboardType = .numberPad
            default:
                break
            }
        }

    }
//
//    //Addchildren datasource
//    func configureCell(_ data:AddChildrenDataSource)
//    {
//        textField.delegate = self
//        textField.text = data.value
//        self.errorLbl.text = ""
//        addChildrenDatasource = data
//        textField.placeholder = data.title
//        textField.isSecureTextEntry = data.cellSecureText
//    }
//
//    //Change password
//    func configureCell(_ data:ChangePasswordDataSource)
//    {
//        textField.delegate = self
//        self.errorLbl.text = ""
//        textField.text = data.value
//        changePasswordDataSource = data
//        textField.placeholder = data.title
//        textField.isSecureTextEntry = data.cellSecureText
//    }
//
//    //parent contact info
//       func configureCell(_ data:ParentsContactInfoDataSource)
//       {
//            textField.isUserInteractionEnabled = false
//           textField.delegate = self
//           self.errorLbl.text = ""
//           textField.text = data.value
//           contactInfoDataSource = data
//           textField.placeholder = data.title
//           textField.isSecureTextEntry = data.cellSecureText
//       }
    
    
    //MARK:-  Selector method
    @objc func tapToDone() {
        self.endEditing(true)
    }
    
    
    //MARK:-  Textfield delegate method
    @IBAction func valueChanged(_ sender:UITextField) {
        
        if let _ = formDataSource {
            if let _ = delegate , let text = sender.text
            {
                self.delegate?.valueChanged(formDataSource!, text)
            }
            
        }
        
        if let _ = signUpDataSource {
            if let _ = delegate , let text = sender.text
            {
                self.delegate?.signUpValueChanged(signUpDataSource!, text)
            }

        }
//
//        if let _ = addChildrenDatasource {
//            if let _ = delegate , let text = sender.text
//            {
//                self.delegate?.valueChangedForAddChildren(addChildrenDatasource!, text)
//            }
//
//        }
//
//        if let _ = changePasswordDataSource {
//            if let _ = delegate , let text = sender.text
//            {
//                self.delegate?.valueChangeForChangePassword(changePasswordDataSource!, text)
//            }
//
//        }
//
//        if let _ = contactInfoDataSource {
//            if let _ = delegate , let text = sender.text
//            {
//                self.delegate?.valueChangeContactInfo(contactInfoDataSource!, text)
//            }
//
//        }
        
        func validatePhnNumber(_ newLength:Int)-> Bool
        {
            if newLength > 12 {
                return false
            }
            return true
        }
        
        func validatePasswordLength(_ newLength:Int)-> Bool {
            if newLength > 15 {
                return false
            }
            return true
        }
        
        func validatePostalCodeLength(_ newLength:Int)-> Bool {
            if newLength > 4 {
                return false
            }
            return true
        }
        
        func validateAccountNumberLength(_ newLength:Int)-> Bool {
            if newLength > 22 {
                return false
            }
            return true
        }
        
        func validatePrice(_ newLength:Int)-> Bool {
            if newLength > 3 {
                return false
            }
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        checkEmailTOShowAlert(textField)
        return true
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if let data = signUpDataSource
//        {
//            if let _ = delegate
//            {
//                switch data {
//                case .dob:
//                    self.delegate?.dobSelectedInSignUp(data)
//                    break
//                case .selectTimeZone:
//                  //  self.delegate?.selectTimeZoneInSignUp(data)
//                    break
//                default:
//                    break
//                }
//            }
//        }
//
//        if let data = addChildrenDatasource
//           {
//               if let _ = delegate
//               {
//                   switch data {
//                   case .dob:
//                       self.delegate?.dobSelectedInAddChildren(data)
//                       break
//                   default:
//                       break
//                   }
//               }
//           }
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
//    {
//        if let data = signUpDataSource
//        {
//            if let _ = delegate
//            {
//                switch data {
//                case .dob:
//                     return true;
//                case .selectTimeZone:
//                    self.delegate?.selectTimeZoneInSignUp(data)
//                    return false
//                default:
//                    return true;
//                }
//            }
//        }
//        return true;
//    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkEmailTOShowAlert(textField)
    }
    
    func checkEmailTOShowAlert(_ field : UITextField)
      {
          if let data = formDataSource
          {
              switch data {
              case .email:
                  if let text = data.user?.username
                  {
                      if text.length > 0
                      {
                          errorLbl.isHidden = TAHelper.isValidEmail(text)
                          errorLbl.isHidden ? (textField.errorMessage = "") : (textField.errorMessage = textField.placeholder)
                          return
                      }
                  }
              default:
                  break
              }
          }
      }
}
