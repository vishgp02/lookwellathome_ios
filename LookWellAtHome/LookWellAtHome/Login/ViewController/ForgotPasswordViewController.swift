//
//  ForgotPasswordViewController.swift
//  ToothFARE
//
//  Created by Vishal Gupta on 23/11/19.
//  Copyright © 2019 Techahead. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txtField: UITextField!
    var loginVM:LoginModeling?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        recheckVM()
        txtField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func recheckVM() {
        if self.loginVM == nil {
            self.loginVM = LoginVM()
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func forgotPasswordApiCall() {
        if TAUtility.toString(object:  txtField.text) == "" {
            TAUtility.showToastMessage(message: AppStrings.email.localized , view: self.view)
            return
        }
        let dictData = ["email":TAUtility.toString(object: txtField.text)] as [String:AnyObject]
        self.loginVM?.ForgotPassword(dictData, completionHandler: { (success) in
            if success ?? false {
                self.moveToOTPScreen()
            }
        })
        
    }
    
    func moveToOTPScreen() {
        
        guard let objOTP = kStoryboardLogin.instantiateViewController(withIdentifier: OTPViewController.className) as? OTPViewController else {
            return
        }
        objOTP.email = TAUtility.toString(object: txtField.text)
        self.navigationController?.pushViewController(objOTP, animated: true)
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        forgotPasswordApiCall()
    }
    
    @IBAction func tapCross(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
