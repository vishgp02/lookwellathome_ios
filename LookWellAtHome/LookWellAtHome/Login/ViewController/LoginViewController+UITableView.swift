//
//  LoginViewController+UITableView.swift
//  ToothFARE
//
//  Created by Vishal Gupta on 18/11/19.
//  Copyright © 2019 Techahead. All rights reserved.
//

import UIKit

extension LoginViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataSource = self.dataSource else { return 0 }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataSource![indexPath.row] as LoginVCDataSource
        switch data.cellType {
        default: return getLoginInputCell(indexPath, tableView: tableView)
        }
    }
    
    fileprivate func getLoginInputCell(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        switch indexPath.row {
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ForgotPasswordTVC.className,for: indexPath) as? ForgotPasswordTVC else {
                fatalError("Unexpected index path")
            }
            cell.delegate = self
            return cell
            
        default:
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FormTextFieldTVC",for: indexPath) as? FormTextFieldTVC else {
            fatalError("Unexpected index path")
        }
        cell.configureCell(dataSource![indexPath.row])
        cell.delegate = self
        return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 2:
            return 80
        default:
            return UITableView.automaticDimension
        }
    }
}

extension LoginViewController: loginTextFieldDelegate, ForgotPasswordTVCDelegate {
 
    func tapForgotPassword() {
        self.view.endEditing(true)
        guard let objForgotPassword = kStoryboardLogin.instantiateViewController(withIdentifier: ForgotPasswordViewController.className) as? ForgotPasswordViewController else {
            return
        }
        self.navigationController?.pushViewController(objForgotPassword, animated: true)
        
        
    }
    
    func valueChanged(_ data: LoginVCDataSource, _ text: String) {
           switch data {
           case .email: user.username = text
           case .password: user.password = text
           default: break
           }
       }
    
}

