//
//  LoginViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 23/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

 import UIKit
 import SwiftyUserDefaults
 import FBSDKLoginKit
 import GoogleSignIn


 class LoginViewController: UIViewController , GIDSignInDelegate {
     
     @IBOutlet weak var tblView: UITableView!
     @IBOutlet var viewHeader: UIView!
     @IBOutlet var viewFooter: UIView!

     var user: UserLoginModel = UserLoginModel()
     var dataSource: [LoginVCDataSource]?
     var loginModelData: LoginModel?
     var loginVM:LoginModeling?
  
     var istabSet = true
     
     override func viewDidLoad() {
         super.viewDidLoad()
         initialSetup()
         
         // Do any additional setup after loading the view.
     }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
//         user.deviceToken = Defaults[.deviceToken] ?? ""
     }
     
     override var prefersStatusBarHidden: Bool {
         return true
     }
     
     func recheckVM() {
         if self.loginVM == nil {
             self.loginVM = LoginVM()
         }
         
     }
     
 
     
     func initialSetup()
     {
         tblView.tableHeaderView = viewHeader
         tblView.tableFooterView = viewFooter
         dataSource = LoginVCDataSource.allLoginProfileField
         dataSource![0].user = user
         registerCell()
         recheckVM()
         
     }
     
     func registerCell() {
         TAHelper.sharedInstance.registerCell(tbl: self.tblView, nibName: FormTextFieldTVC.className)
         TAHelper.sharedInstance.registerCell(tbl: self.tblView, nibName: ForgotPasswordTVC.className)
     }
     
     func validateEmail() -> Bool {
         return TAHelper.isValidEmail(user.username ?? "")
     }

     
      @IBAction func tapBack(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
      }
     
     @IBAction func tapSignin(_ sender: Any) {
     

         self.view.endEditing(true)
         if user.username == ""
         {
             TAUtility.showToastMessage(message: AppStrings.userName.localized, view: self.view)
             return
         }
         if user.password == ""
         {
             
             TAUtility.showToastMessage(message: AppStrings.enterPassword.localized, view: self.view)
             return
         }
         let isValidEmail =  validateEmail() ? true : false
         if isValidEmail ==  true{
             self.loginApiCall()
         }
         self.tblView.endEditing(true)
         
     }
     
     
     
     @IBAction func tapNotMember(_ sender: Any) {
         guard let objSignUp = kStoryboardLogin.instantiateViewController(withIdentifier: SignUpViewController.className) as? SignUpViewController else {
                    return
                }
                self.navigationController?.pushViewController(objSignUp, animated: true)
     }
     
     @IBAction func tapSignup(_ sender: Any) {
         
         guard let objSignUp = kStoryboardLogin.instantiateViewController(withIdentifier: SignUpViewController.className) as? SignUpViewController else {
             return
         }
         self.navigationController?.pushViewController(objSignUp, animated: true)
     }
     
     @IBAction func tapFB(_ sender: Any) {
            self.user.loginType = "1"
            connectWithFB()
        }
        
        @IBAction func tapGoogle(_ sender: Any) {
            //Device Type (0: Manual, 1: Facebook, 2: Instagram, 3: Twitter, 4: Google)
            self.user.loginType = "4"
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().presentingViewController = self
            GIDSignIn.sharedInstance()?.signIn()
        }
        
        
     
     func loginApiCall() {
         
         let dictData = self.loginVM?.getLoginData(user)
         self.loginVM?.LoginApi(dictData!, completionHandler: { (data) in
             if let response = data {
                 self.loginModelData = response
                if TAUtility.toBool(self.loginModelData?.is_verified) == false {
                    self.moveToOTPScreen()
                }
//                 Defaults[.jwtToken] = response.jwttoken
//                 Defaults[.jwtrefreshtoken] = response.jwtrefreshtoken
//                 Defaults[.deviceToken] = response.deviceToken
//                 Defaults[.login] = "login"
//                 Defaults[.parentId] = response.id
               //  TAUtility.showToastMessage(message: "Login Successful", view: self.view)
              
             }
         })
     }

    func moveToOTPScreen() {
           
           guard let objOTP = kStoryboardLogin.instantiateViewController(withIdentifier: OTPViewController.className) as? OTPViewController else {
               return
           }
           objOTP.email = TAUtility.toString(object: user.username)
           self.navigationController?.pushViewController(objOTP, animated: true)
       }
 }


 extension LoginViewController {
     
     func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
         if let error = error {
             print("\(error.localizedDescription)")
         } else {
             // Perform any operations on signed in user here.
             let userId = user.userID                  // For client-side use only!
             let idToken = user.authentication.idToken // Safe to send to the server
             let fullName = user.profile.name
             let givenName = user.profile.givenName
             let familyName = user.profile.familyName
             let email = user.profile.email
             let url = user.profile.imageURL(withDimension: 200)
             if TAUtility.toString(object: email) == "" {
                 TAUtility.showOkAlert(title: "", message: "Login/ registration with this Facebook/ Google account is not possible as the Email ID associated with your account cannot be retrieved. Please use the login/ registration form to proceed")
                 return
             }
             self.user.username = TAUtility.toString(object: email)
             self.user.firstName = TAUtility.toString(object: givenName)
             self.user.lastName = TAUtility.toString(object: familyName)
             self.user.googleId = TAUtility.toString(object: userId)
             self.dataSource![0].user = self.user

             if let url = url {
                 //self.user.profilePic = url.absoluteString
             }
             self.tblView.reloadData()
         }
     }
     
     // This function is called when the user disconnects from the app
     func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
         // Perform any operations when the user disconnects from app here.
         print("User has disconnected")
     }
     
     private func connectWithFB() {
         let fbLoginManager: LoginManager = LoginManager()
         fbLoginManager.logIn(permissions: ["email"], from: self) { result, error in
             if error == nil {
                 let fbloginresult: LoginManagerLoginResult = result!
                 if fbloginresult.grantedPermissions.contains("email") {
                     if (AccessToken.current) != nil {
                         GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { [weak self](_, result, error) -> Void in
                             if error == nil, let result = result as? [String: AnyObject]{
                                 
                                 self?.user.username = result["email"] as? String ?? ""
                                 self?.user.firstName = result["first_name"] as? String ?? ""
                                 self?.user.lastName = result["last_name"] as? String ?? ""
                                 self?.user.email = result["email"] as? String ?? ""
                                 self?.user.facebookId = result["id"] as? String ?? ""
                                 self?.dataSource![0].user = self?.user
                                 if TAUtility.toString(object: self?.user.email) == "" {
                                     
                                     TAUtility.showOkAlert(title: "", message: "Login/ registration with this Facebook/ Google account is not possible as the Email ID associated with your account cannot be retrieved. Please use the login/ registration form to proceed")
                                     return
                                 }
                                 if let imageUrlString = ((result["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                                   //  self?.user.profilePic = imageUrlString
                                 }
                                 //self?.loginApiCall()
                                 self?.tblView.reloadData()
                             } else {
                                 print("error")
                             }
                         })
                     }
                 }
             }
         }
     }
 }

