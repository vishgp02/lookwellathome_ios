//
//  SceneDelegate.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 21/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

 @available(iOS 13.0, *)
 class SceneDelegate: UIResponder, UIWindowSceneDelegate, UNUserNotificationCenterDelegate {

     
     var window: UIWindow?
    

 //    class var windowSceneDelegate: UIWindowSceneDelegate {
 //        if #available(iOS 13.0, *) {
 //            return UIApplication.shared.delegate as! UIWindowSceneDelegate
 //        } else {
 //            // Fallback on earlier versions
 //        }
 //      }
 //
   
     func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
         // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
         // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
         // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
         if #available(iOS 13.0, *) {
             guard let _ = (scene as? UIWindowScene) else { return }
         } else {
             // Fallback on earlier versions
         }
        setSplashScreen()
//         Defaults[.deviceToken] = "123"
//         AMTabView.settings.tabColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//         AMTabView.settings.ballColor = UIColor.clear
//         AMTabView.settings.selectedTabTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//         AMTabView.settings.unSelectedTabTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//         // Chnage the animation duration
//         AMTabView.settings.animationDuration = 1
//         setSplashScreen()
         UNUserNotificationCenter.current().delegate = self
         UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (_, _) in
             DispatchQueue.main.async {
                 // For iOS 10 data message (sent via FCM
                 UIApplication.shared.registerForRemoteNotifications()
             }
         })
        
     }

    func setSplashScreen() {
         guard let obj = kStoryboardLogin.instantiateViewController(withIdentifier: LandingPageViewController.className) as? LandingPageViewController else {
                return
            }
            let navigation = UINavigationController(rootViewController: obj)
            navigation.isNavigationBarHidden = true
            window?.rootViewController = navigation
            window?.makeKeyAndVisible()
            
        }
     // MARK: - UINotification Delegate
       func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
           Debug.log.info("token is : \(token)")
//            Defaults[.deviceToken] = token.lowercased()
       }

       func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//           Defaults[.login] = "1234"
           Debug.log.error("failed to get token")
       }

     /// Manage push notification registration
     func registerForPushNotifications(application: UIApplication) {
         UNUserNotificationCenter.current().delegate = self
         UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (_, _) in
             DispatchQueue.main.async {
                 // For iOS 10 data message (sent via FCM
                 UIApplication.shared.registerForRemoteNotifications()
             }
         })
     }

       func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
           let userInfo = response.notification.request.content.userInfo
           if let info = userInfo as? [String: AnyObject] {
               Debug.log.info(info)
               completionHandler()
           }
       }

       func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
           completionHandler([.alert, .badge, .sound])
           let userInfo = notification.request.content.userInfo
           if let info = userInfo as? [String: AnyObject] {
               Debug.log.info(info)

           }
           
       }

     
     @available(iOS 13.0, *)
     func sceneDidDisconnect(_ scene: UIScene) {
         // Called as the scene is being released by the system.
         // This occurs shortly after the scene enters the background, or when its session is discarded.
         // Release any resources associated with this scene that can be re-created the next time the scene connects.
         // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
     }

     @available(iOS 13.0, *)
     func sceneDidBecomeActive(_ scene: UIScene) {
         // Called when the scene has moved from an inactive state to an active state.
         // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
     }

     @available(iOS 13.0, *)
     func sceneWillResignActive(_ scene: UIScene) {
         // Called when the scene will move from an active state to an inactive state.
         // This may occur due to temporary interruptions (ex. an incoming phone call).
     }

     @available(iOS 13.0, *)
     func sceneWillEnterForeground(_ scene: UIScene) {
         // Called as the scene transitions from the background to the foreground.
         // Use this method to undo the changes made on entering the background.
     }

     @available(iOS 13.0, *)
     func sceneDidEnterBackground(_ scene: UIScene) {
         // Called as the scene transitions from the foreground to the background.
         // Use this method to save data, release shared resources, and store enough scene-specific state information
         // to restore the scene back to its current state.
     }

    

 }
