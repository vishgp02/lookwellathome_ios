//
//  LandingPageViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 26/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController {

    @IBOutlet var btnSignup: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignup.layer.borderWidth = 1
        btnSignup.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }
    

    @IBAction func tapSignIn(_ sender: Any) {
        guard let objSignUp = kStoryboardLogin.instantiateViewController(withIdentifier: LoginViewController.className) as? LoginViewController else {
            return
        }
        self.navigationController?.pushViewController(objSignUp, animated: true)
    }
    
    @IBAction func tapSignUp(_ sender: Any) {
            guard let objSignUp = kStoryboardLogin.instantiateViewController(withIdentifier: SignUpViewController.className) as? SignUpViewController else {
                       return
                   }
                   self.navigationController?.pushViewController(objSignUp, animated: true)
        }
    
}
