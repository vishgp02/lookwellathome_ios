//
//  CreatePasswordViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 26/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class CreatePasswordViewController: UIViewController {
    
    var email = ""
    var loginVM:LoginModeling?
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recheckVM()
        password.text = ""
        confirmPassword.text = ""
        // Do any additional setup after loading the view.
    }
    
    func recheckVM() {
           if self.loginVM == nil {
               self.loginVM = LoginVM()
           }
           
       }
       
    @IBAction func tapBack(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
          
      }
    
    @IBAction func tapSubmit(_ sender: Any) {
        updatePasswordAPiCall()
        
    }

    func updatePasswordAPiCall() {
        
        if TAUtility.toString(object: password.text) == "" || TAUtility.toString(object: confirmPassword.text)  == "" {
            TAUtility.showToastMessage(message: "Please enter password", view: self.view)
            return
        }
        
        if TAUtility.toString(object: password.text) != TAUtility.toString(object: confirmPassword.text) {
            TAUtility.showToastMessage(message: "Please doesn't match", view: self.view)
            return
        }
        
        let dict = ["email": email,
        "password": TAUtility.toString(object: password.text)] as [String:AnyObject]
        
        self.loginVM?.updatePassword(dict, completionHandler: { (success) in
            if success ?? false {
                self.moveToConfirmScreen()
            }
            
        })
    }
    
    func moveToConfirmScreen() {
        guard let objSignUp = kStoryboardLogin.instantiateViewController(withIdentifier: PasswordResetConfirmViewController.className) as? PasswordResetConfirmViewController else {
            return
        }
        self.navigationController?.pushViewController(objSignUp, animated: true)
    }
}
