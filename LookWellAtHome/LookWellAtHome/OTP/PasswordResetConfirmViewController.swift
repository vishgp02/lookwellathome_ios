//
//  PasswordResetConfirmViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 26/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class PasswordResetConfirmViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        moveToLoginScreen()
    }
    
    func moveToLoginScreen() {
        guard let objLogin = kStoryboardLogin.instantiateViewController(withIdentifier: LoginViewController.className) as? LoginViewController else {
            return
        }
        self.navigationController?.pushViewController(objLogin, animated: true)
    }
    
    
}
