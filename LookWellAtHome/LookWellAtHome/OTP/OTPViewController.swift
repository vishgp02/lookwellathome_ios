//
//  OTPViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 26/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import KWVerificationCodeView


class OTPViewController: UIViewController {
    
    @IBOutlet weak var optVW: KWVerificationCodeView!
    @IBOutlet weak var buttonResend: UIButton!
    var loginVM:LoginModeling?
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optVW.digits = 4
        optVW.textSize = 25
        recheckVM()
        // Do any additional setup after loading the view.
    }
    
    func recheckVM() {
        if self.loginVM == nil {
            self.loginVM = LoginVM()
        }
        
    }
    
    
    /// Reset Password Action
    @IBAction func tapVerify(_ sender: Any) {
        self.view.endEditing(true)
        if optVW.hasValidCode() {
            print("true")
            validateOtpApiCall()
        }else {
            TAUtility.showToast(message: "Please enter OTP")
            return
        }
        
    }
    
    @IBAction func tapResendOTP(_ sender: Any) {
        
        let dict = ["email": email] as [String:AnyObject]
             
        self.loginVM?.resendOTP(dict, completionHandler: { (success) in
            
            TAUtility.showToastMessage(message: "OTP Sent", view: self.view)
        })
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func moveToCreatePassword() {
        
        guard let objPass = kStoryboardLogin.instantiateViewController(withIdentifier: CreatePasswordViewController.className) as? CreatePasswordViewController else {
            return
        }
        objPass.email =  email
        self.navigationController?.pushViewController(objPass, animated: true)
    }
    
    func validateOtpApiCall() {
        
        let dict = ["email": TAUtility.toString(object: email),
                    
                    "otp": TAUtility.toInt(optVW.getVerificationCode())] as [String:AnyObject]
        
        self.loginVM?.validateOTP(dict, completionHandler: { (success) in
            if success ?? false {
                self.moveToCreatePassword()
            }
        })
    }
}


