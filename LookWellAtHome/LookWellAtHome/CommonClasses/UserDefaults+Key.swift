//
//  UserDefaults+Key.swift
//  OneNation
//
//  Created by TechAhead on 15/11/17.
//  Copyright © 2017 Techahead Software. All rights reserved.
//

import SwiftyUserDefaults
import UIKit

extension DefaultsKeys {
    static let deviceToken = DefaultsKey<String?>("deviceToken")
    static let authToken = DefaultsKey<String?>("authToken")
    static let jwtToken = DefaultsKey<String?>("jwtToken")
    static let childName = DefaultsKey<String?>("childName")
    static let jwtrefreshtoken = DefaultsKey<String?>("jwtrefreshtoken")
    static let parentId = DefaultsKey<Int?>("id")
    static let emailId = DefaultsKey<String?>("emailId")
    static let firstTime = DefaultsKey<String?>("firstTime")
    static let login = DefaultsKey<String?>("login")
    static let childLogin = DefaultsKey<String?>("isChildlogin")
    static let child_id = DefaultsKey<String?>("child_id")
}
