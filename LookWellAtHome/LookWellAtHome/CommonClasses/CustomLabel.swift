//
//  CustomLabel.swift
//  Venue
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Techahead Softwares. All rights reserved.


import UIKit

class CustomLabel: UILabel {

    override  func awakeFromNib() {
    super.awakeFromNib()
    self.setup()
    }
    
    func setup() {
    let screenRect:CGRect = UIScreen.main.bounds
    let screenWidth:CGFloat = screenRect.size.width
    let scalefactor:CGFloat = screenWidth / 375.0
    self.font = UIFont(name: (self.font?.fontName)!, size: (self.font.pointSize)*scalefactor)
    }
    
    func setupCustomFont(fnt:UIFont) {
        let screenRect:CGRect = UIScreen.main.bounds
        let screenWidth:CGFloat = screenRect.size.width
        let scalefactor:CGFloat = screenWidth / 375.0
        self.font = UIFont(name: (fnt.fontName), size: (fnt.pointSize)*scalefactor)
    }
    
}

class UnderlinedLabel: UILabel {
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.length)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}
