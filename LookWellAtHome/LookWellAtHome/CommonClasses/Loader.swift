//
//  Loader.swift
//  CRIIIO
//
//  Created by Sachin on 01/12/19.
//  Copyright © 2018 TechAhead. All rights reserved.
//

import Foundation

import SVProgressHUD

import ReachabilitySwift


class Loader {

    

    // MARK: - Loading Indicator

    

    static let thickness = CGFloat(6.0)

    static let radius = CGFloat(22.0)

    

     class func showLoader(message: String?) {

            

            DispatchQueue.main.async {

                //TAUtility.hideLoader()

                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)

                SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
                SVProgressHUD.show(withStatus: message!)
//                if message != nil && message != kPleaseWait {
//
//                    SVProgressHUD.show(withStatus: message!)
//
//                } else {
//
//                    SVProgressHUD.show()
//
//                }

            }

        }



    

    class func showLoaderInView(title: String = "Loading...", view: UIView) {

        DispatchQueue.main.async {

            SVProgressHUD.setBackgroundColor(UIColor.clear)

            SVProgressHUD.setDefaultMaskType(.clear)

            SVProgressHUD.setRingThickness(thickness)

            SVProgressHUD.setRingRadius(radius)

            if !SVProgressHUD.isVisible() {

                SVProgressHUD.show()

            }

        }

    }

    

  class func hideLoader() {


    

           DispatchQueue.main.async {

               SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.none)

               SVProgressHUD.dismiss()

               kAppDelegate?.window?.isUserInteractionEnabled = true

           }

           DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {

               kAppDelegate?.window?.isUserInteractionEnabled = true

           }

       }
    

    class func hideLoaderInView(view: UIView) {

        DispatchQueue.main.async {

            SVProgressHUD.dismiss()

        }

    }

    

    // MARK: - Reachability method

    class func isReachabile() -> Bool {

        let reachability = Reachability.init(hostname: "www.google.com")

        return reachability!.isReachable

    }

}
