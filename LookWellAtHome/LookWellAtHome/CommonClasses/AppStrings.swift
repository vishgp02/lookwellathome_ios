//
//  AppStrings.swift
//  ToothFARE
//
//  Copyright © 2018 TechAhead. All rights reserved.
//

import Foundation

enum AppStrings: String {
    case AppName = "LookWellAtHome"
    case emailBlankAlert = "Email field cannot be left blank"
    case passwordBlankAlert = "Password cannot be left blank"
    case enterValidEmail = "Please enter a valid email id"
    case enterValidPassword = "Please enter minimum 8 characters with 1 smallcase, 1 uppercase and 1 number"
    case firstNameBlankAlert = "First Name field cannot be left blank"
    case lastNameBlankAlert = "Last Name field cannot be left blank"
    case enterReferalCode = "Please enter a referral code"
    case passwordNotMatch = "password doesn't match"
    case GuestTriggerMessage = "please login with your criiio account to access this feature"
    case SomethingWentWrongMsg = "Something went wrong, Please try again"
    case enterSubject = "Please enter subject"
    case enterFeedback = "Please enter feedback"
    case feedbackSaved = "Feedback saved successfully"
    case profileUpdate = "Profile updated successfully"
    case accountDelete = "your account deletion request has been submitted successfully"
    case termsAndCondition = "Please accept terms and conditions"
    case loginAgainPasswordChange = "Password updated successfully. Please login again"
    case enterCode = "Please enter code"
    case verificationCode = "Please check your email to get verification code to reset your password"
    case otp = "OTP sent successfully"
    case enterCountry = "Please enter country"
    case firstNameLimit = "First name should be maximum 30 characters only"
    case lastNameLimit = "Last name should be maximum 30 characters only"
    case lastNickNameLimit = "user name should be maximum 30 characters only"
    case selectGender = "Please select gender"

    case noInternet = "no internet connection"
    case backOnline = "back online"
    case confirmPassword = "Please enter confirm password"
    case newPassword = "Please enter new password"
    case currentPassword = "Please enter current password"
    case password = "Please enter minimum 8 characters with 1 smallcase,1 uppercase, and 1 number"
    case email = "Please enter email"
    case enterPassword = "Please enter password"
    case selectTimeZone = "Please select timezone"
    case passwordValidation = "Password must contain at least 4 characters"

    case dob = "Please enter date of birth"
    case firstName = "Please enter first name"
    case lastName = "Please enter last name"
    case userName = "Please enter user name"
    case postalCode = "Please enter postal code"
    case name = "Please enter name"
    case passCode = "Please enter Passcode"

    case consentMessage = "consent is already sent to provided parent's email, Please get it approved"
    case featureComingSoon = "feature comiiing soon..."
    case subjectLimit = "subject should not exceed from 50 characters"
    case feedbackLimit = "feedback should not exceed from 100 characters"
    case appliedReferralCode = "referral code applied successfully"
    case goBack = "Are you sure you want to go back?"
    case providerUserIsMissing = "providerUserId is missing"
    case someThingWentWrong = "something went wrong, Please close the application and open again"
    case verificationCodeLimit = "verification code must be 6 characters long"
    case locationSetting = "please allow location service in settings"
    case whoops = "whoops!"
    case selectDownTimeFirst = "please select downtime first"
    case removePlayer = "are you sure you want to remove this player?"
    case removeMember = "are you sure you want to remove this member?"
    case removeGroup = "are you sure you want to remove this group?"
    case ratingError = "please rate the game to submit rating"
    case selectPlayer = "please select players upto 100"
    case discoverability = "users who are 16 or older can use this feature"

}

extension AppStrings {
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
