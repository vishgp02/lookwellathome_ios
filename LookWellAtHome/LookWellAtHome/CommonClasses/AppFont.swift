//
//  AppFont.swift
//  CRIIIO
//
//  Created by Jobin John on 01/12/19.
//  Copyright © 2018 TechAhead. All rights reserved.
//

import Foundation
import UIKit

private let familyName = "Gilroy-Medium"

enum AppFont: String {
    case bold = "Gilroy-Black"
    case semiBold = "Gilroy-Bold"
    case medium = "Gilroy-Medium"

    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size + 1.0) {
            return font
        }
        fatalError("Font '\(fullFontName)' does not exist.")
    }

    fileprivate var fullFontName: String {
        return rawValue.isEmpty ? familyName : rawValue
    }

}

struct ScreenSize{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}


