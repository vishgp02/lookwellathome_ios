//
//  Debug.swift
//  CRIIIO
//
//  Created by Sachin on 09/01/19.
//  Copyright © 2019 TechAhead. All rights reserved.
//

import Foundation
import Log

class Debug {
    static var log = Logger(formatter: .minimal, theme: nil, minLevel: .info)

    class func Log<T>(message: T, functionName: String = #function, fileNameWithPath: String = #file, lineNumber: Int = #line ) {
        //Configure logging enable by setting true/false
        let isLoggingEnabled = true
        log.enabled = isLoggingEnabled
//        TAThreads.performTaskInBackground {
//            let fileNameWithoutPath: String = NSURL(fileURLWithPath: fileNameWithPath).lastPathComponent ?? ""
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "HH:mm:ss.SSS"
//            let output = "\r\n❗️\(fileNameWithoutPath) => \(functionName) (line \(lineNumber), at \(dateFormatter.string(from: NSDate() as Date)))\r\n"
//            if isLoggingEnabled {
//                Debug.log.info("Output info: ", output)
//            }
//            log.trace("Called!!!")
//            log.debug("Who is self:", self)
//            log.info( message)
//            log.warning(message, separator: " - ")
//            log.error(message, terminator: "😱😱😱\n")
//        }
    }
}
