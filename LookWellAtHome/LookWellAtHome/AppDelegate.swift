//
//  AppDelegate.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 21/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

import FBSDKLoginKit
import GoogleSignIn



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate {
    
   
    
     
     var window: UIWindow?
     class var delegate: AppDelegate {
         return UIApplication.shared.delegate as! AppDelegate
     }
     
     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         // Override point for customization after application launch.
       
         registerForPushNotifications(application: application)
//         GIDSignIn.sharedInstance().clientID = "470914119905-e5p9ofvrleel34b4iglh254ldrppnrfm.apps.googleusercontent.com"
//         GIDSignIn.sharedInstance().delegate = self
//         application.applicationIconBadgeNumber = 0
//         GADMobileAds.sharedInstance().start(completionHandler: nil)
//         GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["kGADSimulatorID"]
        setSplashScreen()
         return true
     }
     
    
    func setSplashScreen() {
        guard let obj = kStoryboardLogin.instantiateViewController(withIdentifier: LandingPageViewController.className) as? LandingPageViewController else {
               return
           }
           let navigation = UINavigationController(rootViewController: obj)
           navigation.isNavigationBarHidden = true
           window?.rootViewController = navigation
           window?.makeKeyAndVisible()
           
       }
    
    
     func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

     }
  
     // MARK: - UINotification Delegate
     func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
         let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
         Debug.log.info("token is : \(token)")
        // Defaults[.deviceToken] = token.lowercased()
     }

     func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
         //Defaults[.deviceToken] = "1234"
         Debug.log.error("failed to get token")
     }
 
     /// Manage push notification registration
     func registerForPushNotifications(application: UIApplication) {
         if #available(iOS 10.0, *) {
             UNUserNotificationCenter.current().delegate = self
             UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (_, _) in
                 DispatchQueue.main.async {
                     // For iOS 10 data message (sent via FCM
                     UIApplication.shared.registerForRemoteNotifications()
                 }
             })
         } else {
             //If user is not on iOS 10 use the old methods we've been using
             let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
             application.registerUserNotificationSettings(notificationSettings)
         }
     }

     @available(iOS 10.0, *)
     func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
         let userInfo = response.notification.request.content.userInfo
         if let info = userInfo as? [String: AnyObject] {
             Debug.log.info(info)
             completionHandler()
         }
     }

     @available(iOS 10.0, *)
     func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
         completionHandler([.alert, .badge, .sound])
         let userInfo = notification.request.content.userInfo
         if let info = userInfo as? [String: AnyObject] {
             Debug.log.info(info)

         }
         
     }

     
     
     
    
     
     // MARK: UISceneSession Lifecycle

     @available(iOS 13.0, *)
     func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
         // Called when a new scene session is being created.
         // Use this method to select a configuration to create the new scene with.
         if #available(iOS 13.0, *) {
             return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
         } else {
             return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
             // Fallback on earlier versions
         }
     }
     
     @available(iOS 13.0, *)
     func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
         // Called when the user discards a scene session.
         // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
         // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
     }
     
     
     

 }


