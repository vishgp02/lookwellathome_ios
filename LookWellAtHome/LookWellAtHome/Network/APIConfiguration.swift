//
//  APIConfiguration.swift
//  
//


import Foundation
import Alamofire

//let kToken = "token"
//let kSuccess = "Success"
//let kMessage = "Message"
//let kResult = "Result"
//let kErrorCode = "errCode"
//let kAuthToken = "token"
//let kCreatedOn = "createdOn"
//let kExpiresIn = "expiresIn"
//let kData = "data"

let KClientSecretKey = "client-secret"
let KClientSecretValue = "TveHJp1mGINShFkYEG33IKCHmDpP7a19pMmlTKXvGlfrGqE6OvaKSfcyAUaI3bP"
let KAcessTokenKey = "access-token"
//let KAcessTokenValue = AppConfig.shared.accessToken ?? ""//UserDefaults.standard.value(forKey:KAcessTokenKey)

let KAcessTokenValue = ""

let KHeaderUserId = "user_id"
let KDeviceID = "1234"

enum APIEnvironment: String
{
    case BaseDev = "https://lookwellathome.in/wp-json/"
    
}


enum ConstantTextsApi: String {
    case noInternetConnection = "No Internet Connection"
    case noInternetConnectionTryAgain = "No Internet Connection. Please try again."
    case connecting =  "connecting"
    case errorOccurred          =  "ErrorOccurred"
    case AppName = "ToothFARE"
    case serverNotResponding =  "Server is not responding."
    case cancel = "Cancel"
    case checkInternetConnection = "Please check your internet connection."
    var localizedString:String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}



