//
//  APIEndPoint.swift
//  ToothFare
//
//  Created by Vishal Gupta on 04/09/19.
//  Copyright © 2019 ToothFare. All rights reserved.
//


import Foundation
import Alamofire

enum APIEndpoint {
    case none
    case createAccount(param: APIParams)
    case forgotPassword(param: APIParams)
    case validateOTP(param: APIParams)
    case verifyAccountOTP(param: APIParams)
    case resendOTP(param: APIParams)
    case updatePassword(param: APIParams)



    case parentProfile(param: APIParams)
    case Login(param: APIParams)
//    case getToothDetail(parentId: String, childId: String, toothNo: String)
//    case getPiggyBankList

     


    
   
    
  

}

