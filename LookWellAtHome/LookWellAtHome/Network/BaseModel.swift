//
//  BaseModel.swift
//  OneNation
//
//  Created by Ashish Chauhan on 11/6/17.
//  Copyright © 2017 Techahead Software. All rights reserved.
//

import Foundation
import ObjectMapper

protocol BaseModeling {
    func apiManagerInstance() -> APIManager?
    
}

extension BaseModeling {
    func apiManagerInstance() -> APIManager? {
        return APIManager.shared
    }
}

class CommanApi: BaseModeling {
    
}

