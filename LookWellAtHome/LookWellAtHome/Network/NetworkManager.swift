//
//  NetworkManager.swift
//  ToothFare
//
//  Created by Pankaj Kumar on 16/09/19.
//  Copyright © 2019 Pankaj Kumar. All rights reserved.
//

import Alamofire
class NetworkManager {
    var messageLabel: UILabel?
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")

    var isInternetConnected = true {
        didSet {
            self.showNetworkMessage(isConnected: isInternetConnected)
        }
    }
    
    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { status in
            switch status {
            case .notReachable:
                Debug.log.info("The network is not reachable")
                if self.isInternetConnected {
                    self.isInternetConnected = false
                }
            case .unknown :
                Debug.log.info("It is unknown whether the network is reachable")
            case .reachable(.ethernetOrWiFi), .reachable(.wwan) :
                Debug.log.info("The network is reachable over the WiFi/WWAN connection")
                if !self.isInternetConnected {
                    self.isInternetConnected = true
                    APIManager.shared.reHitHoldAPI()
                }
            }
        }
        // start listening
        reachabilityManager?.startListening()
    }
    
    
    func showNetworkMessage(isConnected: Bool) {
        if let window = UIApplication.shared.keyWindow {
            
            let bottomSafeAreaInsets = window.safeAreaInsets.bottom
            let labelHeight = bottomSafeAreaInsets + 20.0
            if isConnected {
                if let messageLabel = messageLabel {
                    //                    messageLabel = UILabel(frame: CGRect(x: 0, y: Device.height, width: Device.width, height: 20))
                    messageLabel.text = "back online"
                    messageLabel.textAlignment = .center
                    messageLabel.textColor = .white
                    messageLabel.backgroundColor = .green
                    //                    window.addSubview(messageLabel)
                    UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {[weak self] () in
                        self?.messageLabel?.frame = CGRect(x: 0, y: Device.height, width: Device.width, height: labelHeight)
                    }){[weak self] (bool) in
                        if !(self?.isInternetConnected ?? false) {
                            self?.messageLabel?.removeFromSuperview()
                            self?.messageLabel = nil
                        }
                    }
                    
                } else {
//                    let label = UILabel(frame: CGRect(x: 0, y: Device.height-20, width: Device.width, height: 20))
//                    label.text = "no internet connection"
//                    label.textAlignment = .center
//                    label.textColor = .white
//                    label.backgroundColor = .red
//                    window.addSubview(label)
//                    messageLabel = label
                }
                
                
                
                
            } else {
                if let messageLabel = messageLabel {
//                    messageLabel = UILabel(frame: CGRect(x: 0, y: Device.height, width: Device.width, height: 20))
                    messageLabel.text = "no internet connection"
                    messageLabel.textAlignment = .center
                    messageLabel.textColor = .white
                    messageLabel.backgroundColor = .red
//                    window.addSubview(messageLabel)
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                        messageLabel.frame = CGRect(x: 0, y: Device.height-labelHeight, width: Device.width, height: labelHeight)
                    })
                } else {
                    let label = UILabel(frame: CGRect(x: 0, y: Device.height, width: Device.width, height: labelHeight))
                    label.text = "no internet connection"
                    label.textAlignment = .center
                    label.textColor = .white
                    label.backgroundColor = .red
                    window.addSubview(label)
                    messageLabel = label
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                        label.frame = CGRect(x: 0, y: Device.height-labelHeight, width: Device.width, height: labelHeight)
                    })
                }
            }
            
           
        }
    }
    
}
