//
//  APIRouter.swift
//  ToothFare
//
//  Created by Vishal Gupta on 04/09/19.
//  Copyright © 2019 ToothFare. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import SwiftyUserDefaults

public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String: AnyObject]

struct APIRouter: URLRequestConvertible {
    
    var endpoint: APIEndpoint
    var detail: APIDetail
    init(endpoint: APIEndpoint) {
        
        self.endpoint = endpoint
        self.detail = APIDetail.init(endpoint: endpoint)
    }
    
    var baseUrl: String {
        //AppConfig.shared.baseUrlServer ?? ""//
       return APIEnvironment.BaseDev.rawValue
    }
    
    var method: Alamofire.HTTPMethod {
        
        return detail.method
    }
    
    var path: String {
        
        return detail.path
    }
    
    var parameters: APIParams {
        
        return detail.parameter
    }
    
    var encoding: ParameterEncoding? {
        
        return detail.encoding
    }
    
    var showAlert: Bool {
        
        return detail.showAlert
    }
    
    var showMessageOnSuccess: Bool {
        
        return detail.showMessageOnSuccess
        
    }
    
    var showLoader: Bool {
        
        return detail.showLoader
    }
    
    var supportOffline: Bool {
        
        return detail.supportOffline
    }
    
    var isBaseUrlNeedToAppend: Bool {
        
        return detail.isBaseUrlNeedToAppend
    }
    
    var isHeaderTokenRequired: Bool {
        return detail.isHeaderTokenRequired
    }

    var isAuthenticationRequired: Bool {
       return true
    }
    
    var headerType: HeaderType {
        return detail.headerType
    }

    /// Returns a URL request or throws if an `Error` was encountered.
    /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
    /// - returns: A URL request.
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: baseUrl)
        var urlRequest = URLRequest(url: isBaseUrlNeedToAppend ? (url?.appendingPathComponent(self.path))! : URL(string: self.path)!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60
        switch self.headerType {
        case .anonymous:
            break
        case .authenticated:
            urlRequest.setValue(TAUtility.toString(object:""), forHTTPHeaderField:"Authorization")

           // urlRequest.setValue(TAUtility.toString(object: Defaults[\.jwtToken]), forHTTPHeaderField:"Authorization")
        case .aws:
            break
        }
        urlRequest.setValue("application/json", forHTTPHeaderField:"Content-Type")
//        urlRequest.setValue("g4AK#kf$Zv&p9-Ax", forHTTPHeaderField:"x-api-token")
        print(urlRequest)
        return try encoding!.encode(urlRequest, with: self.parameters)
    }
}
