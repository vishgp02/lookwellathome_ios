//
//  APIManager.swift
//  ToothFare
//
//  Created by Vishal Gupta on 04/09/19.
//  Copyright © 2019 ToothFare. All rights reserved.
//


let KResult = "Result"
let KSatus = "status"
let KSuccess = "Success"
let KMessage = "message"
let KEmail = "email"
let KUserId = "id"


import Alamofire
//import AlamofireImage
import ReachabilitySwift
import DataCache
import Toaster


class APIManager: Alamofire.SessionManager {
    
    internal static let shared: APIManager = {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60
        return APIManager()
    }()
    var arrOfHitsAPI = [[String: Any]]()
    var requestCache = [String: RequestCaching]()
    var requestDic = [String: DataRequest]()
        
    //MARK: - Request
    func request(apiRouter: APIRouter,completionHandler: @escaping (_ responseData: Any, _ success: Bool,_ message: String) -> Void) {
        
        let key = getUniqueForRequest(apiRouter)
        if Loader.isReachabile() == false {
            
            if apiRouter.supportOffline, let data = DataCache.instance.readData(forKey: key) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                    if let reponse = json, let result = reponse["result"] , let message = reponse[KMessage] as? String {
                        completionHandler(result, true, message)
                    }
                } catch {
                    Debug.log.error("error")
                }
            } else {
                if apiRouter.headerType != .aws {
                    TAUtility.showOkAlert(title: "", message: AppStrings.noInternet.localized)
                }
            }
            return
        } //Return if found no network or use cache value
        if apiRouter.showLoader == true { Loader.showLoader(message: kPleaseWait) } //Start loader
        
        TAThreads.performTaskAfterDealy(0.1) {
            
            if !apiRouter.path.isEmpty {
                Debug.log.info( "🔴 TA Request => \(apiRouter.baseUrl)/\(apiRouter.path) : \(String(describing: apiRouter.parameters)) ☄️")
            }
            
            // Create Key
            
            let requestCaching = RequestCaching(apiRouter: apiRouter, completionHandler: completionHandler)
            self.requestCache[key] = requestCaching
            
            
            // Send request to server
            let req = self.request(apiRouter).responseJSON { (responseData: DataResponse<Any>) in
                self.requestDic.removeValue(forKey: key) // Removed excuted request
                
                Debug.log.info("response: \(responseData)")
                Debug.log.info("response: \(String(describing: responseData.response?.statusCode))")
                print("The Status Code is ---->",responseData.response?.statusCode)
                let statusCode = responseData.response?.statusCode
                if let url = responseData.request?.url {
                    if let data = responseData.result.value as? [String:AnyObject] {
                        
                        Debug.log.info("🔴Response In JSON => \(url) =>\n \(TAUtility.toJsonString(from: data)! as NSString) ☄️")

                        
//                       Debug.log.info("🔴Response In JSON => \(url) =>\n \(Helper.toJsonString(data as AnyObject) as NSString) ☄️")

                        Debug.log.info( "🔴Response Of => \(url) =>\n \(data) ☄️")
                    }
                }
                
                if responseData.response == nil {
                    if apiRouter.supportOffline, let data = DataCache.instance.readData(forKey: key) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                            if let reponse = json, let result = reponse[KResult], let message = reponse[KMessage] as? String{
                                completionHandler(result, true, message)
                            }
                        } catch {
                            Debug.log.error("error")
                        }
                    } else {
                        completionHandler([String: AnyObject](), false, ConstantTextsApi.serverNotResponding.localizedString)
                        TAUtility.showOkAlert(title:"", message: ConstantTextsApi.serverNotResponding.localizedString)
                    }
                    
                    if apiRouter.showLoader == true { // Hide loader
                        Loader.hideLoader()
                    }
                    return
                }
                
                var dataObject = [String: AnyObject]()
                if let responseData = responseData.result.value as? [String:AnyObject] {
                    dataObject = responseData
                } else if let responseData = responseData.result.value {
                    dataObject = ["response": responseData as AnyObject]
                }
                
                if let status = dataObject[KSuccess] as? Bool, status == true
                {
                    self.removeHoldAPIRequestApi(apiRouter.path)
                    if let message = dataObject["Message"] as? String {
                        if let result = dataObject[KResult] {
                            completionHandler(result, true, message)
                            } else   {
                            completionHandler(dataObject, true, message)
                        }
                    }
                    if apiRouter.showMessageOnSuccess, let msg = dataObject[KMessage] as? String {
                        
//                        TAUtility.showToast(message: msg)
                        let topViewController = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
                       // TAUtility.showToastMessage(message: msg, view: topViewController.view)

                        
                        
                    }
                    
                    if apiRouter.supportOffline, let data = responseData.data {
                        DataCache.instance.write(data: data, forKey: key)
                    }
                    
                    //DataCache.instance.write(data: dataObject, forKey: key)
                    
                } else {
                    
                    if apiRouter.headerType == .aws {
                        switch statusCode {
                        case 200:
                            completionHandler(dataObject, true, "")
                        default:
                            completionHandler([:], false, "something went wrong")
                        }
                    } else if apiRouter.showAlert == true, let msg = dataObject[KMessage] as? String {
                        if let errorCode = dataObject["code"] as? Int  {
                            switch errorCode {
                            case 406:
                                TAUtility.showOkAlert(title:AppStrings.AppName.localized, message: msg, handler: { (alertAction) in
                                    if alertAction.title == "Ok" {
                                        
                                    }
                                })
                            case 501,502:
 
                                break
                            default:
                                print("")
                            }
                            completionHandler([:], false, msg)
                        } else if statusCode == 501 || statusCode == 502 {
                            
                        }
                        else {
                        }
                        completionHandler([:], false, msg)
                    }
                }
                if apiRouter.showLoader == true { // Hide loader
                    Loader.hideLoader()
                }
            }
            
            // Save request
            self.requestDic[key] = req
        }
    }
    
    //MARK: - Request
    
    func requestForMultiPart(apiRouter: APIRouter,_ arrImage:[UIImage],completionHandler: @escaping (_ responseData: Any, _ success: Bool,_ message:String) -> Void) {
        
        let key = getUniqueForRequest(apiRouter)
        if Loader.isReachabile() == false {
            
            if apiRouter.supportOffline, let data = DataCache.instance.readData(forKey: key) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                    if let reponse = json, let result = reponse[KResult], let message = reponse[KMessage] as? String {
                        completionHandler(result, true, message)
                    }
                } catch {
                    Debug.log.error("error")
                }
            } else {
                TAUtility.showOkAlert(title:"", message: AppStrings.noInternet.localized)
            }
            return
        } //Return if found no network or use cache value
        if apiRouter.showLoader == true { Loader.showLoader(message: kPleaseWait) } //Start loader
        
        TAThreads.performTaskAfterDealy(0.1) {
            
            if !apiRouter.path.isEmpty {
                Debug.log.info("🔴 TA Request => \(apiRouter.baseUrl)/\(apiRouter.path) : \(String(describing: apiRouter.parameters)) ☄️")
            }
            
            // Create Key
            let requestCaching = RequestCaching(apiRouter: apiRouter, completionHandler: completionHandler)
            self.requestCache[key] = requestCaching
            
            // Send request to server
             self.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in apiRouter.parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                var imageName = "profileImage"
                for index in 0..<arrImage.count {
                    if apiRouter.path == "KUploadDocuments" {
                        if index == 0 {
                           imageName = "imageFrontSideURL"
                        }else {
                           imageName = "imageBackSideURL"
                        }
                    }
                    let fileName = "image"
                    let image = arrImage[index]
                    if let imageData =  image.jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(imageData, withName: imageName, fileName: fileName, mimeType: "image/jpeg")
                    }
                }
                Debug.log.info(multipartFormData)
                
            }, with: apiRouter, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        switch response.result {
                            
                        case .success(let json):
                            Debug.log.trace("TA :: Success with JSON: \(json)")
                            self.requestDic.removeValue(forKey: key) // Removed excuted request
                            Debug.log.info( "response: \(json)")
                            

                            if let url = response.request?.url {
                                if let data = response.result.value as? [String:AnyObject] {
                                    Debug.log.info("🔴Response In JSON => \(url) =>\n \(TAUtility.toJsonString(from: data)! as NSString) ☄️")
                                    Debug.log.info( "🔴Response Of => \(url) =>\n \(data) ☄️")
                                }
                            }
                            
                            if response.response == nil {
                                if apiRouter.supportOffline, let data = DataCache.instance.readData(forKey: key) {
                                    do {
                                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                                        if let reponse = json, let result = reponse[KResult], let message = reponse[KMessage] as? String {
                                            completionHandler(result, true, message)
                                        }
                                    } catch {
                                        Debug.log.error("error")
                                    }
                                } else {
                                    completionHandler([String: AnyObject](), false, ConstantTextsApi.serverNotResponding.localizedString)
                                    TAUtility.showOkAlert(title:"", message: ConstantTextsApi.serverNotResponding.localizedString)
                                }
                                
                                if apiRouter.showLoader == true { // Hide loader
                                    Loader.hideLoader()
                                }
                                return
                            }
                            var dataObject = [String: AnyObject]()
                            if let responseData = response.result.value as? [String:AnyObject] {
                                dataObject = responseData
                            } else if let responseData = response.result.value {
                                dataObject = ["response": responseData as AnyObject]
                            }
                            
                            if let success = dataObject[KSuccess] as? Bool, success == true
                            {
                                self.removeHoldAPIRequestApi(apiRouter.path)
                                if let result = dataObject[KResult] {
                                    completionHandler(result, success, "")
                                } else {
                                    completionHandler([:], success, "")
                                }
                                if apiRouter.showMessageOnSuccess, let _ = dataObject[KMessage] as? String {
                                    // TAUtility.showOkAlert(title: ConstantTextsApi.AppName.localizedString, message: msg)
                                }
                                
                                if apiRouter.supportOffline, let data = response.data {
                                    DataCache.instance.write(data: data, forKey: key)
                                }
                                
                                //DataCache.instance.write(data: dataObject, forKey: key)
                                
                            } else {
                                
                                if apiRouter.showAlert == true, let msg = dataObject[KMessage] as? String , let result = dataObject[KResult] ,let success = dataObject[KSuccess] as? Bool , success == false {
                                    //,errorCode == 201 || errorCode == 206
                                    if let errorCode = dataObject["Status"] as? Int,errorCode == 201 || errorCode == 206 { //for user already exist and 206 for otp not verified {
                                        TAUtility.showOkAlert(title:"", message:msg)
                                        completionHandler(result, success, msg)
                                    } else if let errorCode = dataObject["Status"] as? Int, errorCode == 406 {
                                        self.holdAPIRequestApi(apiRouter: apiRouter, completionClosure: completionHandler)
                                    }
                                    else {
                                        TAUtility.showOkAlert(title:"", message: msg)
                                    }
                                    completionHandler([:], false, msg)
                                }
                                //                    return
                            }
                            
                            if apiRouter.showLoader == true { // Hide loader
                                Loader.hideLoader()
                            }
                    
                case .failure(let encodingError):
                    Debug.log.error("TA :: Failure with Error: \(encodingError.localizedDescription)")
                    completionHandler([:], false, "\(encodingError.localizedDescription)")
                    // Save request
                    self.requestDic[key] = upload }
                    }
                case .failure(let encodingError):
                    Debug.log.error("TA :: Failure with Error: \(encodingError.localizedDescription)")
                    completionHandler([:], false, "\(encodingError.localizedDescription)")
                }
            })
            
        }
    }
  
    
    
    internal func requestApiWithMultiPart(_ apiRouter:APIRouter,serviceName: String, imageArray: [UIImage],postData: Parameters, completionClosure: @escaping (_ result: Any?, _ error : Error?) -> ()) -> Void {
        
        let serviceUrl = apiRouter.baseUrl + "/" + serviceName
        
        NSLog("Connecting to Host with URL %@%@ jsonPara String: %@", apiRouter.baseUrl, serviceName, postData)
        var headers: [String: String] = [:]
        // Add AES authentication ...........
//        if let token = Defaults[\.jwtToken] {
//            headers = [KLanguage:L102Language.currentAppleLanguage(),KHeaderToken:token]
//        }
        headers = ["header":"change"]

        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                
                for (key, value) in postData {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                for index in 0..<imageArray.count {
                    let imageName = "profileImage"
                    let fileName = "image"
                    let image = imageArray[index]
                    if let imageData = image.jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(imageData, withName: imageName, fileName: fileName, mimeType: "image/jpeg")
                    }
                }
                Debug.log.info(multipartFormData)
        },
            
            to: serviceUrl,
            
            headers: headers,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        switch response.result {
                            
                        case .success(let json):
                            
                            Debug.log.info("TANetworkManager :: Success with JSON: \(json)")
                            
                            let data: NSData = NSData(data: response.data!)
                            
                            do {
                                
                                let anyObj = try JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
                                
                                Debug.log.info("TANetworkManager :: Success with Result -> \(anyObj!)")
                                
                                completionClosure(anyObj, nil)
                                
                            }
                                
                            catch let error as NSError {
                                
                                completionClosure(nil, error)
                                
                                Debug.log.info("TANetworkManager :: Json Error: \(error.localizedDescription)")
                                
                            }
                            
                        case .failure(let error):
                            
                            Debug.log.info("TANetworkManager :: Failure with Error: \(error.localizedDescription)")
                            
                            completionClosure(nil, error)
                            
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    
                    Debug.log.info("TANetworkManager :: Failure with Error: \(encodingError.localizedDescription)")
                    
                    completionClosure(nil, encodingError)
                    
                }
                
        }
            
        )
        
    }
    
    
    func holdAPIRequestApi(apiRouter: APIRouter, completionClosure:  @escaping (_ responseData: Any, _ success: Bool,_ message: String) -> Void){
        
        var holdDic = [String:Any]()
        holdDic["apiRouter"] = apiRouter
        holdDic["callBack"] = completionClosure
        arrOfHitsAPI.append(holdDic)
    }
    
    func reHitHoldAPI() {
        for holdDic in arrOfHitsAPI {
            guard let apirouter = holdDic["apiRouter"] as? APIRouter,
                let completionClosure = holdDic["callBack"] as? (Any, Bool, String)->(Void)  else{ continue}
            self.request(apiRouter: apirouter, completionHandler: completionClosure)
        }
    }
    
    func removeHoldAPIRequestApi(_ serviceName: String){
        let index = arrOfHitsAPI.index { (holdDic) -> Bool in
            guard let apiRouter = holdDic["apiRouter"] as? APIRouter, apiRouter.path == serviceName  else{return false}
            return true
        }
        if let tempIdex = index, tempIdex < arrOfHitsAPI.count{
            arrOfHitsAPI.remove(at: tempIdex)
        }
    }
    
    func cancelAllRequests() {
        requestCache.removeAll()
        let allKeys = requestDic.keys
        for key in allKeys {
            if let safeRequest = requestDic[key] {
                safeRequest.cancel()
                requestDic.removeValue(forKey: key)
            }
        }
        // Clear request dic
        requestDic.removeAll()
    }
    
    func dataObject(response: DataResponse<Any>) -> [String: AnyObject] {
        var data = [String: AnyObject]()
        if let dataDict = response.result.value as? [String: AnyObject] {
            data = dataDict
        }
        else if let dataArr = response.result.value as? [AnyObject] {
            data = ["response": dataArr as AnyObject]
        }
        return data
    }
    
    func onError(message: String, success: Bool, dataObject: [String: AnyObject], errorCode: Int, showAlert: Bool,  completionHandler: @escaping ([String: AnyObject], Bool) -> ()) {
        
        if !showAlert {
            completionHandler(dataObject, success)
            return
        }
        
        let target = TAUtility.topMostViewController(rootViewController: TAUtility.rootViewController())
        if errorCode != 0 {
            
           // Alert.showAlert(message, okButtonTitle: ConstantTexts.cancel.localized, target: target)
        }
        completionHandler(dataObject, success)
    }

    
    func getUniqueForRequest(_ apiRouter: APIRouter) -> String {
        var key = "\(apiRouter.baseUrl)/\(apiRouter.path)"
        for (k,v) in Array(apiRouter.parameters).sorted(by: {$0.0 < $1.0}) {
            key.append(k)
            key.append("\(v)")
        }
        key = key.replacingOccurrences(of: "//", with: "_")
        key = key.replacingOccurrences(of: "/", with: "_")
        key = key.replacingOccurrences(of: ",", with: "_")
        key = key.replacingOccurrences(of: "@", with: "_")
        key = key.replacingOccurrences(of: ":", with: "_")
        key = key.replacingOccurrences(of: ".", with: "_")
        return key
    }
    
    
    func handleError(_ responseData: [String: AnyObject]){
        
    }
    
    private class func handleProgress(
        progress: CGFloat,
        completionProgress: @escaping(_ progress: CGFloat) -> Void) {
        completionProgress(progress)
    }
}

struct ApiMessages {
    
    static let NoInternetConnection = ConstantTextsApi.noInternetConnection.localizedString
    static let Connecting = ConstantTextsApi.connecting.localizedString
    static let APIResponseError  = ConstantTextsApi.errorOccurred.localizedString
    
    /*
    static func APIResponseError(statusCode: Int?, errorCode: Int) -> String {
        let statusCode = statusCode != nil ? String(statusCode!) :ConstantTexts.None.localizedString
        return " \(ApiMessages.APIResponseError)\n \(ConstantTexts.InternalCode.localized): \(errorCode)\n\(ConstantTexts.StatusCode.localized): \(statusCode)"
    }*/
}

struct RequestCaching {
    var apiRouter: APIRouter
    var completionHandler: ((_ responseData: [String: AnyObject], _ success: Bool,_ message: String) -> Void)?
}

