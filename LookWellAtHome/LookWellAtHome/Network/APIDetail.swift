//
//  APIDetail.swift
//  ToothFare
//
//  Created by Vishal Gupta on 04/09/19.
//  Copyright © 2019 ToothFare. All rights reserved.
//

import Foundation
import Alamofire
//import SwiftyUserDefaults
//import ObjectMapper

public enum HeaderType: String {
    case anonymous      = "anonymous"
    case authenticated  = "authenticated"
    //case azureHeader    = "azureHeader"
    //case hybridHeader   = "hybridHeader"
    //    case none           = "none"
    case aws            = "aws"
}

struct APIDetail {
    
    var path: String = ""
    var parameter: APIParams = APIParams()
    var method: Alamofire.HTTPMethod = .get
    var encoding: ParameterEncoding = URLEncoding.default
    var isBaseUrlNeedToAppend: Bool = true
    var showLoader: Bool = true
    var showAlert: Bool = true
    var showMessageOnSuccess = false
    var isHeaderTokenRequired: Bool = true
    var supportOffline = false
    var headerType:HeaderType = .anonymous
    
    init(endpoint: APIEndpoint) {
        
        switch endpoint {
            
            
        //new apis
        case .Login(let param):
            path = "jwt-auth/v1/token"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
        case .createAccount(let param):
            path = "wp/v2/users/register"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
        case .forgotPassword(let param):
            path = "wp/v2/users/forget-password"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
        case .validateOTP(let param):
            path = "wp/v2/users/validate-otp"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
        case .verifyAccountOTP(let param):
            path = "wp/v2/users/verify-account-otp"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
            
        case .resendOTP(let param):
            path = "wp/v2/users/resend-otp"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
        case .updatePassword(let param):
            path = "wp/v2/users/update-password"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            headerType = .anonymous
            showAlert = true
            showLoader = true
            showMessageOnSuccess = true
            
            //
            //        case .deleteChild(let param):
            //            path = "parent/deleteChild"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //
            //
            //        case .parentLogin(let param):
            //            path = "parent/login"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //
            //        case .parentProfile(let param):
            //            path = "parent/myProfile"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //
            //
            //        case .saveAvatar(let param):
            //            path = "child/saveAvatar"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //        case .getBrokenToothList:
            //            path = "/child/getBrokenToothList"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //
            //        case .changePassword(let param):
            //            path = "parent/changePassword"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //        case .parentForgotPassword(let param):
            //            path = "parent/forgetPassword"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //            showMessageOnSuccess = true
            //
            //        case .childLogin(let param):
            //            path = "child/login"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //            showLoader = true
            //
            //            showMessageOnSuccess = true
            //
            //
            //        case .addChildren(let param):
            //            path = "parent/addChild"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //            showMessageOnSuccess = true
            //
            //        case .getChildrenList(let param):
            //            path = "parent/getChildList"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //        case .updateChild(let param):
            //            path = "parent/updateChild"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //
            //        case .updateProfile(let param):
            //            path = "parent/updateProfile"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //        case .sendSimpleMessage(let param):
            //            path = "parent/sendSimpleMessage"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //
            //        case .parentHome:
            //            path = "parent/home"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //
            //        case .getChildBrokenToothList(let id):
            //            let url = "parent/getChildBrokenToothList/" + id
            //            path = url
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //        case .getToothDetail(let id, let childId, let toothNo):
            //            let url = "parent/getToothDetail/" + id + "/" + childId + "/" + toothNo
            //            path = url
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //        case .deleteNotification(let id):
            //            let url = "parent/deleteNotification/" + id
            //            path = url
            //            parameter = [:]
            //            method = .delete
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //        case .childDelMsg(let id):
            //            let url = "child/deleteMessage/" + id
            //            path = url
            //            parameter = [:]
            //            method = .delete
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //
            //
            //
            //        case .sendToothFairyMessage(let param):
            //            path = "parent/sendToothFairyMessage"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //            showMessageOnSuccess = true
            //
            //        case .myMessages:
            //            path = "parent/myMessages"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //            case .myNotification:
            //                path = "parent/myNotification"
            //                parameter = [:]
            //                method = .get
            //                encoding = URLEncoding.default
            //                headerType = .authenticated
            //                showAlert = true
            //                showLoader = true
            //
            //
            //        case .childProfile(let id):
            //            let url = "child/myProfile/" + id
            //            path = url
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //        case .childNotification:
            //            path = "child/myNotification"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //
            //        case .getToothFairyMessages:
            //            path = "child/getToothFairyMessages"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //
            //        case .getPiggyBankList:
            //            path = "child/getMyPiggyBank"
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //            showLoader = true
            //
            //        case .getToothChildrenDetail(let id):
            //            let url = "child/getMyToothDetail/" + id
            //            path = url
            //            parameter = [:]
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //            case .childSendMyTooth(let param):
            //                path = "child/sendMyTooth"
            //                parameter = param
            //                method = .post
            //                encoding = JSONEncoding.default
            //                headerType = .authenticated
            //                showAlert = true
            //                showLoader = true
            //                showMessageOnSuccess = true
            //
            //
            //            ///////------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //            //old apis
            //
            //        case .createAccount(let param):
            //            path = "register"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //        case .forgotPassword(let param):
            //            path = "forgot-password"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //            //        case .checkUser(let param):
            //            //            path = "check-user"
            //            //            parameter = param
            //            //            method = .get
            //            //            encoding = URLEncoding.default
            //            //            headerType = .anonymous
            //            //            showAlert = true
            //
            //        case .facebookLogin(let param):
            //            path = "facebook-login"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //        case .googleLogin(let param):
            //            path = "google-login"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //        case .generateAccessToken(let param):
            //            path = "access-token"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = false
            //
            //        case .verifyOtp(let param):
            //            path = "verify-otp"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //        case .resetPassword(let param):
            //            path = "reset-password"
            //            parameter = param
            //            method = .put
            //            encoding = JSONEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //
            //            //AWS S3 Config file
            //            //        case .downloadConfig:
            //            //            path = S3Helper().getConfigUrlString()
            //            //            isBaseUrlNeedToAppend = false
            //            //            method = .get
            //            //            encoding = URLEncoding.default
            //            //            headerType = .aws
            //            //            showAlert = false
            //            //            showLoader = false
            //
            //        case .termsPolicy(let param):
            //            path = "terms-policy"
            //            parameter = param
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .anonymous
            //            showAlert = true
            //
            //        case .userProfile(let param):
            //            path = "user-profile"
            //            parameter = param
            //            method = .put
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //
            //        case .logout(let param):
            //            path = "logout"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //
            //        case .notifyAdmin(let param):
            //            path = "notify-admin"
            //            parameter = param
            //            method = .post
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //
            //        case .truliooAttemptCount(let param):
            //            path = "trulioo-attempt-count"
            //            parameter = param
            //            method = .put
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            //
            //        case .getProfile(let param):
            //            path = "user-profile"
            //            parameter = param
            //            method = .get
            //            encoding = URLEncoding.default
            //            headerType = .authenticated
            //            showLoader = true
            //            showAlert = true
            //
            //        case .upDateProfile(let param):
            //            path = "user-profile"
            //            parameter = param
            //            method = .put
            //            encoding = JSONEncoding.default
            //            headerType = .authenticated
            //            showAlert = true
            
            
        default:
            break
        }
    }
}
