//
//  SignUpVC+UITableView.swift
//  ToothFARE
//
//  Created by Vishal Gupta on 18/11/19.
//  Copyright © 2019 Techahead. All rights reserved.
//

import UIKit

extension SignUpViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataSource = self.dataSource else { return 0 }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataSource![indexPath.row] as SignUpVcDataSource
        switch data.cellType {
        default: return getLoginInputCell(indexPath, tableView: tableView)
        }
    }
    
     fileprivate func getLoginInputCell(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        switch indexPath.row {
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectGenderAboutTVC.className,for: indexPath) as? SelectGenderAboutTVC else {
                fatalError("Unexpected index path")
            }
            cell.delegate = self
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FormTextFieldTVC",for: indexPath) as? FormTextFieldTVC else {
                fatalError("Unexpected index path")
            }
            cell.configureSignUpView(dataSource![indexPath.row])
            cell.delegate = self
            return cell
        }
        
    }
    
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 5:
            return 80
        default:
            return UITableView.automaticDimension
        }
    }
}

extension SignUpViewController: loginTextFieldDelegate, SelectGenderAboutTVCDelegate {
 
    //SelectGenderAboutTVCDelegate function
      func selectMale(btn: UIButton) {
          user.gender = "m"
      }
      
      func selectFemale(btn: UIButton) {
          user.gender = "f"
      }
    
    
    func signUpValueChanged(_ data: SignUpVcDataSource, _ text: String) {
        switch data {
        case .email: user.email = text
        case .firstName: user.firstName = text
        case .lastName: user.lastName = text
        case .password: user.password = text
        case .confirmPassword: user.confirmPassword = text
        case .username: user.username = text
         case .gender: user.gender = text
        case .phone: user.phone = text
            case .address: user.address = text
            case .city: user.city = text

           

        default: break
        }
    }
    
}
