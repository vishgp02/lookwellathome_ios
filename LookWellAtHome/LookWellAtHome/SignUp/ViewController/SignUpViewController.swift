//
//  SignUpViewController.swift
//  LookWellAtHome
//
//  Created by Vishal Gupta on 26/01/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import Kingfisher
import FBSDKLoginKit
import GoogleSignIn

class SignUpViewController: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewFooter: UIView!
    @IBOutlet var profileImage: UIImageView!

    var dataSource: [SignUpVcDataSource]?
    var user: SignUpDataModel = SignUpDataModel()
    var signupVM:SignUpModeling?

    var tabImage: UIImage? {
        return UIImage(named: "ChildrenUnSelected")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        recheckVM()
        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func recheckVM() {
        if self.signupVM == nil {
            self.signupVM = SignUpVM()
        }
        
    }
    
    func initialSetup()
    {
        tblView.tableFooterView = viewFooter
        dataSource = SignUpVcDataSource.allSignUpField
        dataSource![0].user = user
        registerCell()
        
        GIDSignIn.sharedInstance().clientID = "470914119905-e5p9ofvrleel34b4iglh254ldrppnrfm.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
    }
    
    func registerCell() {
        TAHelper.sharedInstance.registerCell(tbl: self.tblView, nibName: FormTextFieldTVC.className)
        TAHelper.sharedInstance.registerCell(tbl: self.tblView, nibName: SelectGenderAboutTVC.className)
        user.gender = "m"

    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapAlreadyMember(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func tapSignin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func tapUploadImage(_ sender: Any) {
        
        ImgPickerHandler.sharedHandler.getImage(instance: self ) { (image) in
            TAUtility.showLoader(message: kPleaseWait)
            AzureImageUpload.uploadProfileImage(image: image) { (uploadStatus, error) in
                TAUtility.hideLoader()
                TAThreads.performTaskInMainQueue {
                    self.profileImage.image = image
                    self.user.profilePic = uploadStatus ?? ""
                }
            }
        }
    }
    

    
    @IBAction func tapContinue(_ sender: Any) {
        self.view.endEditing(true)
        if user.firstName == ""
        {
            TAUtility.showToastMessage(message: AppStrings.firstName.localized, view: self.view)
            
            return
        }
        if user.lastName == ""
        {
            TAUtility.showToastMessage(message: AppStrings.lastName.localized, view: self.view)
            
            return
        }
        if user.email == ""
        {
            TAUtility.showToastMessage(message: AppStrings.email.localized, view: self.view)
            
            return
        }
        let isValidEmail =  validateEmail() ? true : false
        if isValidEmail ==  false {
            TAUtility.showToastMessage(message: AppStrings.enterValidEmail.localized, view: self.view)
            return
        }
        if user.username == ""
        {
            TAUtility.showToastMessage(message: AppStrings.userName.localized, view: self.view)
            return
        }
        if user.phone == ""
        {
            TAUtility.showToastMessage(message: "Please enter mobile number", view: self.view)
            return
        }
        if user.gender == ""
        {
            TAUtility.showToastMessage(message: AppStrings.selectGender.localized, view: self.view)
            return
        }
        if user.address == ""
        {
            TAUtility.showToastMessage(message: "Please enter address", view: self.view)
            return
        }
        if user.city == ""
        {
            TAUtility.showToastMessage(message: "Please enter city", view: self.view)
            return
        }
        if user.password == ""
        {
            TAUtility.showToastMessage(message: AppStrings.enterPassword.localized, view: self.view)
            return
        }
        if user.confirmPassword == ""
        {
            TAUtility.showToastMessage(message: AppStrings.enterPassword.localized, view: self.view)
            return
        }
        if user.password  != user.confirmPassword {
            TAUtility.showToastMessage(message: AppStrings.passwordNotMatch.localized, view: self.view)
            return
        }
        if TAUtility.toInt(user.password.count) < 4
        {
            TAUtility.showToastMessage(message: AppStrings.passwordValidation.localized, view: self.view)
            return
        }
        signUpAPiCall()
    }
    
    func moveToOTPScreen() {
        
        guard let objOTP = kStoryboardLogin.instantiateViewController(withIdentifier: OTPViewController.className) as? OTPViewController else {
            return
        }
        objOTP.email = TAUtility.toString(object: user.email)
        self.navigationController?.pushViewController(objOTP, animated: true)
    }
    
    func signUpAPiCall() {
        
        if let dictData = self.signupVM?.getSignUpData(self.user) {
            self.signupVM?.signUpApi(dictData, completionHandler: { (success, result) in
                print(result)
                if let signupResult = result as? [String:AnyObject] {
                    if TAUtility.toBool(signupResult["is_verified"]) == false {
                         self.moveToOTPScreen()
                    }
                }
            })
        }
    }
    
    @IBAction func tapFB(_ sender: Any) {
        self.user.loginType = "1"
        connectWithFB()
    }
    
    @IBAction func tapGoogle(_ sender: Any) {
        //Device Type (0: Manual, 1: Facebook, 2: Instagram, 3: Twitter, 4: Google)
        self.user.loginType = "4"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
   
    
    func validateEmail() -> Bool {
        return TAHelper.isValidEmail(user.email)
    }
}

extension SignUpViewController {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let url = user.profile.imageURL(withDimension: 200)
            if TAUtility.toString(object: email) == "" {
                TAUtility.showOkAlert(title: "", message: "Login/ registration with this Facebook/ Google account is not possible as the Email ID associated with your account cannot be retrieved. Please use the login/ registration form to proceed")
                return
            }
            self.user.email = TAUtility.toString(object: email)
            self.user.firstName = TAUtility.toString(object: givenName)
            self.user.lastName = TAUtility.toString(object: familyName)
            self.user.googleId = TAUtility.toString(object: userId)

            if let url = url {
                //self.user.profilePic = url.absoluteString
            }
            self.tblView.reloadData()
        }
    }
    
    // This function is called when the user disconnects from the app
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        print("User has disconnected")
    }
    
    private func connectWithFB() {
        let fbLoginManager: LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { result, error in
            if error == nil {
                let fbloginresult: LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions.contains("email") {
                    if (AccessToken.current) != nil {
                        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { [weak self](_, result, error) -> Void in
                            if error == nil, let result = result as? [String: AnyObject]{
                                
                                self?.user.email = result["email"] as? String ?? ""
                                self?.user.firstName = result["first_name"] as? String ?? ""
                                self?.user.lastName = result["last_name"] as? String ?? ""
                                self?.user.email = result["email"] as? String ?? ""
                                self?.user.facebookId = result["id"] as? String ?? ""
                                
                                if TAUtility.toString(object: self?.user.email) == "" {
                                    
                                    TAUtility.showOkAlert(title: "", message: "Login/ registration with this Facebook/ Google account is not possible as the Email ID associated with your account cannot be retrieved. Please use the login/ registration form to proceed")
                                    return
                                }
                                if let imageUrlString = ((result["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                                  //  self?.user.profilePic = imageUrlString
                                }
                                self?.tblView.reloadData()
                            } else {
                                print("error")
                            }
                        })
                    }
                }
            }
        }
    }
}
