//
//  SignUpVM.swift
//
//
//

import UIKit
import SwiftyUserDefaults

protocol SignUpModeling
{
    func getSignUpData(_ user:SignUpDataModel) -> [String:AnyObject]
    func signUpApi(_ signUpParam: [String: AnyObject],completionHandler: @escaping (_ success:Bool?, _ response:[String:AnyObject]) -> Void)
}

class SignUpVM: SignUpModeling , BaseModeling {
    
    
    
    func getSignUpData(_ user: SignUpDataModel) -> [String : AnyObject] {
        var paramDict = [String:AnyObject]()
        paramDict =
            [
                "email": user.email as AnyObject,
                "first_name": user.firstName as AnyObject,
                "last_name": user.lastName as AnyObject,
                "password": user.password as AnyObject,
                "username": user.username as AnyObject,
                "gender":user.gender as AnyObject,
                "phone_number": user.phone as AnyObject,
                "address": user.address as AnyObject,
                "city": user.city as AnyObject,
                //                "profile_pic": user.profilePic as  AnyObject,
                "device_token":"123" as AnyObject,
                "device_type":kDeviceType as AnyObject,
                "login_type":kLoginType as AnyObject,
                
        ]
        return paramDict
    }
    
    func signUpApi(_ signUpParam: [String : AnyObject], completionHandler: @escaping (Bool?, [String : AnyObject]) -> Void) {
        self.apiManagerInstance()?.request(apiRouter: APIRouter.init(endpoint: .createAccount(param: signUpParam)), completionHandler: { (response, success, message) in
            
            if success
            {
                guard let result = response as? [String: AnyObject] else {return}
                completionHandler(true,result)
                
            }else {
                completionHandler(true,[:])
            }
            
        })
    }
    
    
}
