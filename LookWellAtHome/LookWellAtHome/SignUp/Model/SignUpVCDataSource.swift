//
//  SignUpVCDataSource.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 01/12/19.
//  Copyright © 2018 Vishal Gupta. All rights reserved.
//

import Foundation
enum SignUpVcDataSource
{
    
    case email
    case firstName
    case lastName
    case phone
    
    case password
    case confirmPassword
    
    case username
    case gender
    case address
    case city
    
    
    static let allSignUpField = [firstName,lastName,email,username,phone,gender,address,city,password,confirmPassword]
    
    static var signUpModel : SignUpDataModel?
    var user : SignUpDataModel?
    {
        get{return SignUpVcDataSource.signUpModel}
        set{SignUpVcDataSource.signUpModel = newValue}
    }
    
    enum CellType {
        case textField
        case termField
        case createBtn
        case socialBtn
    }
    
    
    enum keyBoardType
    {
        case email
        case text
        case number
        case postalCode
        
    }
    
    /*
     case .email: return AppStrings.email.localized
     case .firstName: return AppStrings.firstName.localized
     case .lastName: return AppStrings.lastName.localized
     case .password: return AppStrings.enterPassword.localized
     case .username: return AppStrings.userName.localized
     case .postalCode: return AppStrings.email.localized
     case .gender: return AppStrings.selectGender.localized
     case .dob: return AppStrings.dob.localized
     case .financialInstitution: return ""
     case .familyDental: return ""
     case .selectTimeZone: return AppStrings.selectTimeZone.localized //"Email"
     */
    
    var title : String
    {
        switch self {
        case .firstName: return "First Name"
        case .lastName: return "Last Name"
        case .email: return "Email"
        case .password: return "Password"
        case .confirmPassword: return "Confirm Password"
        case .username: return "User Name"
        case .gender: return "Gender"
        case .phone: return "Mobile Number"
        case .address: return "Address"
        case .city: return "City"
            
        default:
            return ""
        }
    }
    
    var value : String
    {
        switch self {
        case .email: return user?.email ?? ""
        case .firstName: return user?.firstName ?? ""
        case .lastName: return user?.lastName ?? ""
        case .password: return user?.password ?? ""
        case .confirmPassword: return user?.confirmPassword ?? ""
            
        case .username: return user?.username ?? ""
        case .gender: return user?.gender ?? ""
        case .phone: return user?.phone ?? ""
        case .address: return user?.address ?? ""
        case .city: return user?.city ?? ""
            
        default:
            return ""
        }
    }
    
    var cellSecureText : Bool
    {
        switch self {
        case .email: return false
        default:
            return true
        }
    }
    
    var keyBoardType : keyBoardType
    {
        switch self {
        case .email: return .email
        case .phone: return .number
        default:
            return .text
        }
    }
    
    var cellType: CellType {
        switch self {
        case .email: return .textField
        case .firstName: return .textField
        case .lastName: return .textField
        case .password : return .textField
        case .username: return .textField
        case .gender: return .termField
        case .phone: return .textField
            case .address: return .textField
            case .city: return .textField

        case .confirmPassword:
            return .textField
        }
    }
    
    enum signUpType:String {
        case  FBSignUp = "FBSignUp"
        case  GoogleSignUp =  "GoogleSignUp"
        case  Other =  "Other"
    }
}

class SignUpDataModel
{
    var email = ""
    var firstName = ""
    var lastName = ""
    var password = ""
    var confirmPassword = ""
    var address = ""
    var city = ""
    
    var username = ""
    var postalCode = ""
    var gender = ""
    var phone = ""
    var financialInstitution = ""
    var profilePic = ""
    var familyDental = ""
    var selectTimeZone = ""
    static var isEmailValid:Bool = true
    static var isPhoneNumberValid:Bool = true
    static var isPasswordValid:Bool = true
    
    var facebookId: String?
    var googleId: String?
    var twitterId: String?
    var socialId = ""
    var loginType = ""
    var deviceToken = ""
    var deviceType = ""
    //var imageUrlString: String?
}

