//
//  VerifyOtp.swift
//  MyFitMap
//
//  Created by Vishal Gupta on 02/12/19.
//  Copyright © 2018 Vishal Gupta. All rights reserved.
//

import UIKit
import ObjectMapper

class SignUpModel:NSObject , Mappable , NSCoding{

    var contactNumber : String? = ""
    var email : String? = ""
    var isOtpVerified : Int?
    var otp : String? = ""
    var userId : Int?
    var token : String? = ""
    var expire : Int?

    required init?(map: Map)
    {
        
    }
    
    func mapping(map: Map)
    {
        contactNumber   <- map["contactNumber"]
        email           <- map["email"]
        isOtpVerified   <- map["isOtpVerified"]
        otp             <- map["otp"]
        userId          <- map["userId"]
        token          <- map["token"]
        expire          <- map["expire"]

    }
    
    required init?(coder aDecoder: NSCoder) {
//        self.contactNumber = aDecoder.decodeObject(forKey: KContactNumber) as? String
//        self.email = aDecoder.decodeObject(forKey:KEmail) as? String
//        self.isOtpVerified = (aDecoder.decodeObject(forKey:KIsOtpVerified) as? Int)!
//        self.otp = aDecoder.decodeObject(forKey:KOTP) as? String
//        self.userId = aDecoder.decodeObject(forKey:KUserId) as? Int
//        self.token = aDecoder.decodeObject(forKey:KJWTToken) as? String
//        self.expire = aDecoder.decodeObject(forKey:KExpireTime) as? Int
    }
    
    func encode(with aCoder: NSCoder) {
//        aCoder.encode(self.contactNumber, forKey:KContactNumber)
//        aCoder.encode(self.email, forKey:KEmail)
//        aCoder.encode(self.isOtpVerified, forKey:KIsOtpVerified)
//        aCoder.encode(self.otp, forKey:KOTP)
//        aCoder.encode(self.userId, forKey:KUserId)
//        aCoder.encode(self.token, forKey:KJWTToken)
//        aCoder.encode(self.expire, forKey:KExpireTime)

    }
    
    
    
    class func formattedData(data: [String: AnyObject]) -> SignUpModel? {
        return Mapper<SignUpModel>().map(JSON:data)
    }
    
}
