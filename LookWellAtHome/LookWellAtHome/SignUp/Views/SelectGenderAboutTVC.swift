//
//  SelectGenderAboutTVC.swift
//  ToothFARE
//
//  Created by Vishal Gupta on 18/11/19.
//  Copyright © 2019 Techahead. All rights reserved.
//

import UIKit

protocol SelectGenderAboutTVCDelegate {
    
    func selectMale(btn:UIButton)
    func selectFemale(btn:UIButton)

}

class SelectGenderAboutTVC: UITableViewCell {

    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var headerLbl : CustomLabel!
    @IBOutlet weak var headerLeadingCnstrnt : NSLayoutConstraint!
    var index : Int!
    var isCreateProfile = false
    
    var isEditProfile = false

    
    var delegate:SelectGenderAboutTVCDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectMale()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func selectMale()
    {
        btnMale.isSelected = true
        btnFemale.isSelected = false
        lblMale.textColor = UIColor.colorWith(hexString: "#1F222E")
        lblFemale.textColor =  UIColor.colorWith(hexString: "#ACB1C0")
    }
    
    func selectFemale()
    {
        btnMale.isSelected = false
        btnFemale.isSelected = true
        lblMale.textColor = UIColor.colorWith(hexString: "#ACB1C0")
        lblFemale.textColor = UIColor.colorWith(hexString: "#1F222E")
    }

    func configureCell(text: String) {
        if isEditProfile {
            btnMale.isUserInteractionEnabled = true
            btnFemale.isUserInteractionEnabled = true
        }else {
            btnMale.isUserInteractionEnabled = false
            btnFemale.isUserInteractionEnabled = false
        }
        
        if text.lowercased() == "m" {
            selectMale()
        }else{
            selectFemale()
        }
    }
    
    @IBAction func selecteUnselect(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.tag == 0
        {
            if let _ = delegate
            {
                self.selectMale()
                self.delegate?.selectMale(btn: sender)
            }
        }else if sender.tag == 1
        {
            if let _ = delegate
            {
                selectFemale()
                self.delegate?.selectFemale(btn: sender)
            }
        }
    }
}
